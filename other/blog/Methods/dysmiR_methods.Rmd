---
html_document:
  fig_caption: yes
author: "Jaime A Castro-Mondragon"
date: 'Last update: `r Sys.Date()`'
output:
  pdf_document: default
  html_document:
    df_print: paged
  fig_caption: yes
bibliography: citations.bib
link-citations: yes
csl: nature.csl
title: "dysmiR Methods and notes"
keep_tex: yes
number_sections: yes
highlight: zenburn
theme: cerulean
toc: yes
toc_depth: 4
toc_float: yes
---

```{r setup, include=FALSE}

## Setup knitr options
library(knitr)
library("data.table")
knitr::opts_chunk$set(echo = FALSE, eval = TRUE, warning = FALSE, results = TRUE, message = FALSE, comment="")


## Specify base directory
dir.main <- "."
setwd(dir.main)



```

\pagebreak
# Introduction



# Methods

All the analyses were done using the human genome *hg19* assembly. In the cases where the data is originally provided in another genome assembly, the coordinates were converted to hg19 using *liftOver*.


## pri-miRNA coordinates

We considered as pri-miRNAs the region encompassing between the 3' end of a pre-miRNA from *miRBase* v20 [@Kozomara2014] and the experimentally validated TSS from *FANTOM5*[@DeRie2017] associated to the same pre-miRNA in the same strand (**Table 1**).


## pre-miRNA coordinates

We used the pre-miRNAs from *miRBase* v20 [@Kozomara2014]. In order to consider mutations in RNA regulatory motifs, which are generally located immediately up-/down-stream the pre-miRNA [@Fromm2016], we extended the pre-miRNAs 30nt to both sides. 

<!-- https://hyperbrowser.uio.no/hb/!mode=basic -->


## Cohorts

The data was obtained from the Breast Cancer Somatic Genetics Study[@Nik-Zainal2016] (*BASIS*) and The Cancer Genome Atlas[@Zhang2015] (*TCGA*) cohorts, we only considered those samples with the following data: (i) Whole-Genome Sequencing (WGS), (ii) RNA-seq, (iii) sRNA-seq, and (iv) Copy Number Alteration (CNA). Microarrays cab be used in place of RNA-seq.

### TCGA

We analyzed a set of seven cohorts (BRCA-US, LIHC-US, UCEC-US, HNSC-US, LUSC-US, LUAD-US, STAD-US) of different cancer types from at least 35 donors, for a total of 343 samples (up to 90 samples per cohort) with the data requirements mentioned above. The selected samples are shown in **Table 2**.


### BASIS 

We selected 296 samples with the data requirements mentioned above. In this cohort, the miRNA expression was measured with a Human miRNA Microarray Slide (see BASIS[@Nik-Zainal2016] publication's supp notes ). The selected samples are shown in **Table 3**.


##  TFBS

We used a collection of computationally predicted TFBSs with experimental evidence of ChIP-seq centrality for 232 TFs across 315 cell types and tissues derived from *UniBind* [@Gheorghe2018].


## Somatic mutations

We considered Single Nucleotide Variants (SNVs) and short indels

### Protein coding genes

We downloaded the Whole Exonic Sequencing (WXS) data for the selected samples from the ICGC portal[@Zhang2011]. We considered only the *loss of function* (LoF) mutations: (i) Nonsense (disruptive inframe deletion, disruptive inframe insertion, stop gained, start lost, stop lost, stop retained variant), (ii) Frame-shift (frameshift variant, initiator codon variant), and (iii) Splice-site (splice region variant, splice donor variant, splice acceptor variant).


### miRNA genes

We downloaded the Whole Genome Sequencing (WGS) data for the selected samples from the ICGC portal[@Zhang2011] via the *icgc-get* client. We used the mutations called by *MuSE*[@Fan2016]. For every sample, we mapped these mutations with the coordinates of the extended pre-miRNAs.


### TFBS

For each sample, we intersected the mutation coordinates with the predicted TFBSs derived from *UniBind*.


## TFBS-gene association

For the *xseq*[@Ding2015] model, in addition to consider the exonic or extended pre-miRNA mutations, we also considered a gene as *mutated* in a given sample if there are mutations at their associated TFBSs. We used the cis-regulatory element (*CRE*)-gene associations from the *GeneHancer* [@Fishilevich2017] database version 4.9 to associate the TFBSs: when a TFBS is within a CRE, we associated the TFBSs with the CRE-associated genes, otherwise the TFBSs are associated to the closest TSS either for protein coding genes *RefSeq Curated* [@Pruitt2002] or miRNA TSS from *FANTOM5* [@DeRie2017].


## RNA-seq and sRNA-seq

For the TCGA cohorts, we downloaded the RNA-seq and sRNA-seq raw counts from the ICGC portal. For *BASIS*, we downloaded the RNA-seq raw counts (for protein coding genes) and the miRNA expression matrix (90th percentile normalized) [@Nik-Zainal2016].

We applied two filters to the raw counts [@Aibar2017]. The first filter, total number of counts per gene, was applied to remove genes unlikely expressed (i.e., mainly 0's in the RNA-seq matrix), we kept the genes with at least 3 counts in at least 5% of the cohort. The second filter, the number of samples where the gene is detected, was applied to remove the genes that were expressed in only a few (noisy) samples. We kept the genes with at least 32/64 raw counts (protein coding/miRNA) in at least 10% of the cohort .

The raw counts of the filtered RNA-seq matrix were converted to counts per millions (*cpm*) using the *cpm* function from the R package *edgeR*[@Robinson2010]. The *cpm* were converted to *log2* scale.

<!-- 

## TFBS filtered based on gene expression

Given that the predicted TFBSs come from distinct conditions, in order to select TFBSs that may potentially dysregulate their targets, we applied a filter based on gene expression [@Mathelier2015]. In every cohort, we calculated the expression mean and standard deviation (sd) of all the genes, we kept those TFBS-gene associations when the TF is mutated in a particular sample and the expression of the target in the same sample is +/- 1sd from the gene mean expression.

-->


## Copy Number Alterations

We downloaded the Copy number Alterations (CNA) tables predicted by GISTIC2 correspondoing to each cohort. The TCGA CNA tables were downloaded through the Firebrowse database at http://firebrowse.org.

For each cohort, we generated a CNA table exclusively for the miRNAs (the original CNA table does not contain information for hundreds of miRNAs listed in mirBase v20[@Kozomara2014]). For each sample, we mapped all the pre-miRNA coordinates within the lessons (amplification/deletion) peaks and assigned the same original and thresholded values from the GISTIC output file *all_lesions.conf_X.txt*.


## Gene networks

### Gene-association network (Protein coding genes)

We used the combined functional gene association network published by Ding *et al*[@Ding2015] (see the methods in the reference) containing 898,032 interactions.

### miRNA-mRNA

We used all the predicted sites for representative transcripts in targetScan v7.2 [@Agarwal2015]. We filtered this network by keeping only those miRNA-gene interactions when a miRNA is targeting a gene in at least two sites [@Krek2005; @Seitz2017]. Since the *xseq* model requires a weighted network (range: 0 to 1)[@Ding2015], we divided the *context++ score* percentiles by 100 and the resulting values were used as the miRNA network weights.


### Update weights based on expression

For every cohort, we update the networks (PPI and miRNA) by testing if the targets of a mutated gene are differentially expressed between the mutated/non-mutated samples with an adjusted P-value (BH correction method) <= 0.05. When this p-value is significant, the weight in the tested interaction is updated to 1, otherwise, we keep the original weight (see methods from Ding *et al*[@Ding2015]).


## Cancer genes GSEA/GO enrichment





\pagebreak
# Table's captions

- **Table 1**: BED file the the coordinates of the human pri-miRNAs in the hg19 version. The pre-miRNA names correspond to those in miRBase v20. **File: Table_1_primiRNAs_F5_hg19.bed**


- **Table 2**: Sample IDs for the selected samples in the TCGA cohorts. **File: Table_2_selected_TCGA_sample_IDs_all_cohorts.tab**


- **Table 3**: Sample IDs and PAM50 classification for the selected samples in the BASIS cohort. **File: Table_3_selected_BASIS_sample_IDs.tab**



<!-- more /storage/mathelierarea/processed/jamondra/To_move_in_raw/miR_BASIS_436_90thperc_norm_sampleIDs.csv | head -n 1 | xargs | perl -lne ' $_ =~ s/\s+/\n/gi; print $_' | sort > BASIS_selected_samples.tab -->


\pagebreak

# Notes

- *miRNAtap* R package does not provide a score that could be used in the weighted network required by *xseq*. For each miRNA-gene pair, it provides a combined rank score among 5 distinct algorithms.


\pagebreak
# References