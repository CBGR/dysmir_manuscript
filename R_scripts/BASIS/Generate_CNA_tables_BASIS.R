#####################
## Load R packages ##
#####################
required.libraries <- c("data.table")

for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    suppressPackageStartupMessages(library(lib, character.only=TRUE))
  }
}


##################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message("; Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


#########################
## Mandatory arguments ##
#########################
if (!exists("results.dir")) {
  stop("Missing mandatory argument (Output directory): results.dir ")
  
} else if (!exists("gistic.lvl4.tab.file")) {
  stop("Missing mandatory argument (GISTIC partitioning level 4 table): gistic.lvl4.tab.file ")
  
} else if (!exists("gistic.lvl4.log.tab.file")) {
  stop("Missing mandatory argument (GISTIC partitioning level 4 log table): gistic.lvl4.log.tab.file ")
  
}
gistic.tables.names <- c("cna", "cna_log")


## Debug
# results.dir <- file.path("/storage/mathelierarea/processed/jamondra/Projects/dysmir/BASIS/TEST")
# gistic.lvl4.tab.file <- "/storage/scratch/TCGA/BASIS/n560_join25_peelYes_cap2_5.all_thresholded.by_genes.txt"
# gistic.lvl4.log.tab.file <- "/storage/scratch/TCGA/BASIS/n560_join25_peelYes_cap2_5.all_data_by_genes.txt"


###########################
## Create results folder ##
###########################
message("; Creating result folders")
out.folders <- list()
out.folders[["CNA"]] <- file.path(results.dir, "CNA")
trash <- sapply(out.folders, dir.create, recursive = TRUE, showWarnings = FALSE)


############################
## Load GISTIC lvl4 table ##
############################
message("; Reading TCGA BRCA GISTIC lvl4 table: ", gistic.lvl4.tab.file)
gistic.lvl4.tab <- fread(gistic.lvl4.tab.file, header = TRUE, sep = "\t")


################################
## Load GISTIC lvl4 log table ##
################################
message("; Reading TCGA BRCA GISTIC lvl4 log table: ", gistic.lvl4.log.tab.file)
gistic.lvl4.log.tab <- fread(gistic.lvl4.log.tab.file, header = TRUE, sep = "\t")


#########################
## Adapt GISTIC tables ##
#########################
gistic.cna <- NULL
gistic.cna.log <- NULL
for(tab in gistic.tables.names){

  gistic.tab <- NULL

  ## Choose a table depending on the iteration
  if(tab == "cna"){
    gistic.tab <- gistic.lvl4.tab

  } else if(tab == "cna_log"){
    gistic.tab <- gistic.lvl4.log.tab
  }


  ## NOTE: the data required for xseq must have the patients in rows and genes in columns.
  gistic.gene.names <- gistic.tab$`Gene Symbol`

  ## Remove additional columns, not required for xseq.
  gistic.tab <- gistic.tab[,-c(1,2,3)]
  gistic.tab <- t(gistic.tab)

  ## Rename columns
  colnames(gistic.tab) <- gistic.gene.names

  ## Rename rows. Parse the BASIS IDs.
  rownames(gistic.tab) <- paste0("X", gsub(rownames(gistic.tab), pattern = "\\D+", replacement = ""))
  
  
  ## Export the parsed tables
  if(tab == "cna"){
    gistic.tab.file <- file.path(out.folders[["CNA"]], "gistic_cna.tab")
  } else if(tab == "cna_log"){
    gistic.tab.file <- file.path(out.folders[["CNA"]], "gistic_cna_log.tab")
  }
  write.table(gistic.tab, file = gistic.tab.file, quote = FALSE, col.names = TRUE, row.names = TRUE, sep = "\t")
}