#####################
## Load R packages ##
#####################
required.libraries <- c("data.table",
                        "xseq")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    library(lib, character.only=TRUE)
  }
}


###################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message(";Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


if (!exists("results.dir")) {
  stop("Missing mandatory argument (Results folder): results.dir ")
} else if (!exists("mut.miR.tab.file")) {
  stop("Missing mandatory argument (Mutation table): mut.miR.tab.file ")
} else if (!exists("expr.miR.tab.file")) {
  stop("Missing mandatory argument (miR expression table): expr.miR.tab.file  ")
} else if (!exists("prefix.name")){
  stop("Missing mandatory argument prefix): prefix.name  ")
} else if (!exists("mut.types")){
  stop("Missing mandatory argument mutation type): mut.types  ")
}


## Set non-mandatory variables (only if they were not declared from the command line)
if (!exists("plot.format")) {
  plot.format <- c("pdf", "png");
}


###########
## Debug ##
###########
# results.dir <- "/storage/mathelierarea/processed/jamondra/Projects/dysmir/TEST/BASIS_PCG_CIS"
# prefix.name <- "PCG"
# mut.tab.file <- "/storage/scratch/TCGA/BASIS/All_mutations_clean.VCF"
# expr.tab.file <- "/storage/scratch/TCGA/BASIS/BASIS_cpm_log2.txt"


###########################
## Create results folder ##
###########################
message("; Creating result folders")
out.folders <- list()
out.folders[["plots"]] <- file.path(results.dir, "plots")
out.folders[["tables"]] <- file.path(results.dir, "tables")
thrash <- sapply(out.folders, dir.create, recursive = TRUE, showWarnings = FALSE)


#########################
## Load mutation table ##
#########################
message("; Reading input tables")
message("; Reading mutation table: ", mut.tab.file)
mut.tab <- fread(mut.tab.file, sep = "\t", header = FALSE)
colnames(mut.tab) <- c("chr", "start", "end", "sample", "Mut_calling_type", "hgnc_symbol", "Mut_type", "Mut_effect", "Mut_effect_detailed")
# dim(mut.tab)

## Keep genes with a hgnc symbol
mut.tab <- mut.tab[!grepl(mut.tab$hgnc_symbol, pattern = "^-$", perl = TRUE), ]

## Rename the sample IDs
mut.tab$sample <- gsub(mut.tab$sample, pattern = "[A-za-z]", replacement = "")
mut.tab$sample <- paste0("X", mut.tab$sample)


###################################
## Load RNA-seq expression table ##
###################################
message("; Reading RNA-seq expression mutation table: ", expr.tab.file)
expr.tab <- read.csv(expr.tab.file, sep = "\t", header = TRUE)

## Rename rows
rownames(expr.tab) <- expr.tab$X
expr.tab <- within(expr.tab, rm(X))


##########################################
## Normalizing data (required) for xseq ##
##########################################
message(";xseq - Estimating gene expression")
## Compute whether a gene is expressed in the studied tumour type. 
## If the expression data are from microarray, there is not need to compute weights.
xseq.weights.pdf <- file.path(out.folders[["plots"]], "BASIS_weights_distrib_xseq_cis_PCG.pdf")
pdf(xseq.weights.pdf)
weight <- EstimateExpression(expr.tab,
                             show.plot = TRUE)
dev.off()


## Impute missing values
expr.tab <- ImputeKnn(expr.tab)

## Quantile-Normalization
## Check this link: https://en.wikipedia.org/wiki/Quantile_normalization
expr.tab.quantile <- QuantileNorm(expr.tab)


######################
## Initialize model ##
######################
message(";xseq - Get expression distribution")
## Show distribution without copy number alterations
expr.dis.quantile <- GetExpressionDistribution(expr=expr.quantile)

# id <- weight[mut.miR.tab[, "hgnc_symbol"]] >= 0.80
# id <- id[!is.na(id)]
# id <- names(id)

ids <- names(weight[which(weight >= 0.7)])

## Select those miR with high expression
mut.filt <- mut.miR.tab[which(mut.miR.tab$hgnc_symbol %in% ids),]

# dim(mut.miR.tab)
# dim(mut.filt)


########################################################################
## Calculate the posterior probabilities according the mutation types ##
########################################################################
message(";xseq - Calculating posterior probabilities")
# mut.types <- c("both", "loss", "gain")
# mut.types <- c("both")
init <- list()
model.cis <- list()
model.cis.em <- list()
xseq.pred <- list()
for(m in mut.types){
  
  message(";xseq - Mutation type: ", m)
  
  ## Set xseq priors
  message(";xseq - Set priors")
  init[[m]] <- SetXseqPrior(expr.dis = expr.dis.quantile,
                            mut <- mut.filt,
                            mut.type <- m,
                            cis <- TRUE)
  
  constraint <- list(equal.fg=FALSE)
  
  ## Initiate model
  message(";xseq - Initiallizing model")
  model.cis[[m]] = InitXseqModel(mut = mut.filt,
                                 expr = expr.quantile,
                                 expr.dis = expr.dis.quantile,
                                 cpd = init[[m]]$cpd,
                                 cis = TRUE,
                                 prior = init[[m]]$prior,
                                 debug = FALSE)
  
  ## EM step
  message(";xseq - Learning parameters (EM)")
  model.cis.em[[m]] = LearnXseqParameter(model = model.cis[[m]],
                                         constraint = constraint,
                                         iter.max = 50,
                                         threshold = 1e-6,
                                         cis = TRUE,
                                         debug = FALSE, 
                                         show.plot = FALSE)

  
  ## Print table with posterior probabilities
  message(";xseq - Exporting table with posterior probabilities")
  xseq.pred[[m]] <- ConvertXseqOutput(model.cis.em[[m]]$posterior)
  colnames(xseq.pred[[m]]) <- c("sample", "miR", "prob_mut", "prob_gene")
}


## Concatenate results
xseq.tab <- data.frame()
## Concatenate the xseq tables
for(m in mut.types){
  
  ## Add a column with the mutation type
  xseq.pred[[m]]$mut_type <- m
  
  ## Concatenate in a new dataframe
  xseq.tab <- rbind(xseq.tab, xseq.pred[[m]])
  
}


## Export xseq results
xseq.tab.file <- file.path(out.folders[["xseq_tables"]], paste(prefix.name, "xseq_posterior_prob_table.tab", sep = "_"))
message(";Exporting xseq result table: ", xseq.tab.file)
write.table(xseq.tab, file = xseq.tab.file , sep = "\t", quote = FALSE, col.names = TRUE, row.names = FALSE)