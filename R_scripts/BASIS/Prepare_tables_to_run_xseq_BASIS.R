#####################
## Load R packages ##
#####################
required.libraries <- c("dplyr")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    library(lib, character.only=TRUE)
  }
}


###################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message(";Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


if (!exists("results.dir")) {
  stop("Missing mandatory argument (Results folder): results.dir ")
} else if (!exists("mut.miR.tab.file")) {
  stop("Missing mandatory argument (Mutation table): mut.miR.tab.file ")
} else if (!exists("expr.miR.tab.file")) {
  stop("Missing mandatory argument (miR expression table): expr.miR.tab.file  ")
} else if (!exists("miR.premiR.tab.file")) {
  stop("Missing mandatory argument (miR <-> pre-miR association table): miR.premiR.tab.file  ")
} else if (!exists("prefix.name")){
  stop("Missing mandatory argument prefix): prefix.name  ")
} else if (!exists("feature.bed.file")){
  stop("Missing mandatory argument (feature BED file)): feature.bed.file")
} else if (!exists("mimat.mir.key.file")){
  stop("Missing mandatory argument (MIMAT <-> miR association table)): mimat.mir.key.file")
} else if (!exists("mimat.mirgendb.file")){
  stop("Missing mandatory argument (MIMAT in mirgendb table): mimat.mirgendb.file")
} else if (!exists("mimat.F5.robust.file")){
  stop("Missing mandatory argument (MIMAT robust mir FANTOM5 table): mimat.F5.robust.file")
} else if (!exists("mimat.mirbase.robust.file")){
  stop("Missing mandatory argument (MIMAT robust mir mirbase table): mimat.mirbase.robust.file")
} else if (!exists("premir.region.tab.file")){
  stop("Missing mandatory argument (Genomic region of pre-miRNAs (intergenic/intronic) ): premir.region.tab.file")
} 


##########################
# pri-miR  data to test ##
##########################
# results.dir <- file.path("results/xseq")
# prefix.name <- "primiRNA"
# mut.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/xseq_primiR_mut_table.tsv"
# expr.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/miR_BASIS_436_90thperc_norm_sampleIDs.csv"
# miR.premiR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/hsa_mir_to_premir.tsv"
# feature.bed.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/primiRs_F5.bed"
# mimat.mir.key.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/key_MIMAT_miRNA.txt"
# mimat.mirgendb.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/mirgeneDB_MIMATs.tab"
# mimat.F5.robust.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/FANTOM5_robust_miRs.tab"
# mimat.mirbase.robust.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/mirbase_robust_MIMATs.tab"
# premir.region.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/premiR_genomic_localization.tab"


##########################################
# pri-miR + SuperEnhancer  data to test ##
##########################################
# results.dir <- file.path("results/xseq")
# prefix.name <- "primiRNA_SuperEnhancer"
# mut.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/Mutations_SuperEnhancer_primir.bed"
# expr.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/miR_BASIS_436_90thperc_norm_sampleIDs.csv"
# miR.premiR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/hsa_mir_to_premir.tsv"
# feature.bed.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/primiRs_F5.bed"
# mimat.mir.key.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/key_MIMAT_miRNA.txt"
# mimat.mirgendb.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/mirgeneDB_MIMATs.tab"
# mimat.F5.robust.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/FANTOM5_robust_miRs.tab"
# mimat.mirbase.robust.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/mirbase_robust_MIMATs.tab"
# premir.region.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/premiR_genomic_localization.tab"


################################
# SuperEnhancer  data to test ##
################################
# results.dir <- file.path("results/xseq")
# prefix.name <- "SuperEnhancer"
# mut.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/SuperEnhancer_test/Mutations_in_SuperEnhancers.bed"
# expr.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/miR_BASIS_436_90thperc_norm_sampleIDs.csv"
# miR.premiR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/hsa_mir_to_premir.tsv"
# feature.bed.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/primiRs_F5.bed"
# mimat.mir.key.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/key_MIMAT_miRNA.txt"
# mimat.mirgendb.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/mirgeneDB_MIMATs.tab"
# mimat.F5.robust.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/FANTOM5_robust_miRs.tab"
# mimat.mirbase.robust.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/mirbase_robust_MIMATs.tab"
# premir.region.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/premiR_genomic_localization.tab"


###########################
## Create results folder ##
###########################
message(";Creating result folders")
results.dir <- file.path(results.dir, prefix.name)
folders.to.create <- list()
folders.to.create[["prefix"]] <- results.dir
folders.to.create[["plots"]] <- file.path(results.dir, "plots")
folders.to.create[["xseq_tables"]] <- file.path(results.dir, "xseq_tables")
thrash <- sapply(folders.to.create, dir.create, recursive = TRUE, showWarnings = FALSE)


##############################
## Load miR mutations table ##
##############################
message(";Reading input tables")
message(";Reading mutation table: ", mut.miR.tab.file)
mut.miR.tab <- read.csv(mut.miR.tab.file, sep = "\t", header = FALSE)
colnames(mut.miR.tab) <- c("hgnc_symbol", "sample", "variant_type", "chr", "pos")
mut.miR.tab$hgnc_symbol <- tolower(mut.miR.tab$hgnc_symbol)
mut.miR.tab$sample <- gsub(pattern = "\\D+", replacement = "", x = mut.miR.tab$sample, perl = TRUE)
mut.miR.tab$sample <- as.character(mut.miR.tab$sample)
# View(mut.miR.tab)


###############################
## Load miR expression table ##
###############################
message(";Reading miR expression mutation table: ", expr.miR.tab.file)
expr.miR.tab <- read.csv(expr.miR.tab.file, sep = "\t", header = TRUE)

## Remove the "X" the sample names - This is added by R
## The sample IDs should be the rownames
colnames(expr.miR.tab) <- as.vector(sapply(names(expr.miR.tab), function(x){ gsub("X", "", x)}))
rownames(expr.miR.tab) <- as.vector(expr.miR.tab[,1])
#rownames(expr.miR.tab) <- as.vector(sapply(rownames(expr.miR.tab), function(x){ gsub("-", "_", x)}))
expr.miR.tab <- expr.miR.tab[,-1]

## Transpose the table - This is required for the xseq input
## Columns (after transposition): miRNA ID
## Rows (after transposition): sample ID
expr.miR.tab <- t(expr.miR.tab)
expr.miR.tab <- as.matrix(expr.miR.tab)
colnames(expr.miR.tab) <- tolower(colnames(expr.miR.tab))
# View(head(expr.miR.tab))


###################
## Load BED file ##
###################
message(";Reading BED file: ", feature.bed.file)
feature.bed <- read.csv(feature.bed.file, sep = "\t", header = FALSE)

## Get the columns with the miR name
mir.col <- as.vector(sapply(feature.bed[1,], function(x){grepl("hsa-", x)}))
mir.col <- mir.col * 1:length(as.vector(feature.bed[1,]))
mir.col <- as.vector(which(mir.col != 0))
feature.bed <- feature.bed[,c(1:3, mir.col)]
colnames(feature.bed) <- c("chr", "start", "end", "premiR")
feature.bed$premiR_ID <- tolower(feature.bed$premiR)
feature.bed <- mutate(feature.bed, feature_width = end - start)
feature.bed <- mutate(feature.bed , width_rank = rank(feature.bed$feature_width))


##########################################
## Load miR - pre-miR association table ##
##########################################
message(";Reading miR-premiR association table: ", miR.premiR.tab.file)
miR.premiR.tab <- read.csv(miR.premiR.tab.file, sep = "\t", header = FALSE)
colnames(miR.premiR.tab) <- c("miR", "premiR")

miR.premiR.tab$miR <- tolower(miR.premiR.tab$miR)
miR.premiR.tab$premiR <- tolower(miR.premiR.tab$premiR)


## Add a new column with the number of times each miR is found on different pre-miR
miR.premiR.tab$nb_miR_dif_loci <- sapply(miR.premiR.tab$miR, function(m){
  length(which(miR.premiR.tab$miR == m))
})

## Add a new column with the premiRs associated to each miR
miR.premiR.tab$premiRs_associated_to_miR <- sapply(miR.premiR.tab$miR, function(m){
  selected.premiR <- as.vector(subset(miR.premiR.tab, miR == m)[,2])
  selected.premiR.print <- paste(selected.premiR, collapse = ",")
})

## Add a new column with the number of miRs within the selected pre-miR
miR.premiR.tab$nb_miR_in_premiR <- sapply(miR.premiR.tab$premiR, function(p){
  length(which(miR.premiR.tab$premiR == p))
})

## Add a new column with the names of the miRs within the selected pre-miR
miR.premiR.tab$miR_in_premiR <- sapply(miR.premiR.tab$premiR, function(p){
  selected.miR <- as.vector(subset(miR.premiR.tab, premiR == p)[,1])
  selected.miR.print <- paste(selected.miR, collapse = ",")
})

## Create a list with the miRNA (content) - pre-miRNA (names) association
miR.to.premiR <- miR.premiR.tab$miR
names(miR.to.premiR) <- miR.premiR.tab$premiR

## Create a list with the pre-miRNA (content) - miRNA (names) association
premiR.to.miR <- miR.premiR.tab$premiR
names(premiR.to.miR) <- miR.premiR.tab$miR


##############################################
## Load the genomic region of premiRs table ##
##############################################
message(";Load the genomic region of premiRs table: ", premir.region.tab.file)
premir.region.tab <- read.csv(premir.region.tab.file, sep = "\t", header = TRUE)
premir.region.tab <- premir.region.tab[,c(1,2,4)]
colnames(premir.region.tab) <- c("promoter", "region", "premiR")
premir.region.tab$premiR <- tolower(premir.region.tab$premiR)

## Obtain the name of the host gene
## The name is parsed from the promoter column when the premiR is intronic and its name does not contain genomic coordinates
## In case of intergenic genes, some of them are associated to non-coding gene TSS (e.g., lnc-RNAs) and not with coordinates.
premir.region.tab$host_gene <- ifelse(premir.region.tab$region == "intronic" & !grepl("chr\\d+", premir.region.tab$promoter, perl = TRUE),
                                      yes = gsub(premir.region.tab$promoter, pattern = "p\\d+@", replacement = "", perl = TRUE),
                                      no = NA)

# premir.region.tab$host_gene <- ifelse(premir.region.tab$region == "intronic" & !grepl("chr\\d+", premir.region.tab$promoter, perl = TRUE),
#                                       yes = gsub(premir.region.tab$promoter, pattern = "p\\d+@", replacement = "", perl = TRUE),
#                                       no = ifelse(!grepl("chr\\d+", premir.region.tab$promoter, perl = TRUE),
#                                                   yes = gsub(premir.region.tab$promoter, pattern = "p\\d+@", replacement = "", perl = TRUE),
#                                                   no = "NA"))

## Merge the premiR genomic region with the miR-premiR table
premir.region.tab <- merge(premir.region.tab, miR.premiR.tab, by = "premiR")[,1:5]


##########################################
## Load MIMAT <-> miR association table ##
##########################################
message(";Reading MIMAT <-> miR association table: ", mimat.mir.key.file)
mimat.mir.key <- read.csv(mimat.mir.key.file, sep = "\t", header = TRUE)
colnames(mimat.mir.key) <- c("MIMAT", "miR")
mimat.mir.key$MIMAT <- tolower(mimat.mir.key$MIMAT)
mimat.mir.key$miR <- tolower(mimat.mir.key$miR)


######################################
## Load MIMAT in FANTOM5 robust set ##
######################################
message(";Reading MIMAT in FANTOM5 robust set: ", mimat.F5.robust.file)
mimat.F5.robust <- read.csv(mimat.F5.robust.file, sep = "\t", header = FALSE)
colnames(mimat.F5.robust) <- c("premiR", "status")
mimat.F5.robust$premiR <- tolower(mimat.F5.robust$premiR)

## the Fantom5 robust set contains the pre-miRs, in order to obtain the miR IDs, the table is merged with the premiR-miR key
mimat.F5.robust <- merge(mimat.F5.robust, miR.premiR.tab, by = "premiR")[,c(3,2)]


######################################
## Load MIMAT in miRbase robust set ##
######################################
message(";Reading MIMAT in miRbase robust set: ", mimat.mirbase.robust.file)
mimat.mirbase.robust <- read.csv(mimat.mirbase.robust.file, sep = "\t", header = FALSE)
colnames(mimat.mirbase.robust) <- c("MIMAT")
mimat.mirbase.robust$MIMAT <- tolower(mimat.mirbase.robust$MIMAT)


############################
## Load MIMAT in mirgenDB ##
############################
message(";Reading MIMAT in mirgenDB robust set: ", mimat.mirgendb.file)
mimat.mirgendb <- read.csv(mimat.mirgendb.file, sep = "\t", header = FALSE)
colnames(mimat.mirgendb) <- c("MIMAT")
mimat.mirgendb$MIMAT <- tolower(mimat.mirgendb$MIMAT)


############################
## miRNA data exploration ##
############################

## Number of miRNAs
nb.miRNAs.total <- nrow(miR.premiR.tab)

## Number of unique miRNAs
nb.miRNAs.uniq <- length(unique(miR.premiR.tab$miR))

## Names and number of the repeated miRNAs
names.repeated.miRNAs <- names(which(table(miR.premiR.tab$miR) > 1))
nb.repeated.miRNas <- length(names(which(table(miR.premiR.tab$miR) > 1)))


################################
## pre-miRNA data exploration ##
################################

## Number of unique pre-miRNAs
nb.premiRNAs.uniq <- length(unique(miR.premiR.tab$premiR))

## Names and number of the repeated pre-miRNAs
names.repeated.premiRNAs <- names(which(table(miR.premiR.tab$premiR) > 1))
nb.repeated.premiRNAs <- length(names(which(table(miR.premiR.tab$premiR) > 1)))


#############################################################################################
## Filtering repeated columns:
## 
## The columns are repeated because the normalization: low expressed miR will be discarded 
#############################################################################################
message("; Detecting low-expressed miRs")
## All the miR IDs are unique
# sum(table(colnames(expr.miR.tab)) > 1)

## ...but some columns have the same values
nb.repeated.values <- as.vector(table(apply(expr.miR.tab,2, sum)))
repeated.sum.of.values <- as.numeric(names(table(apply(expr.miR.tab,2, sum)))[1])
rep.pos <- as.vector(which(apply(expr.miR.tab,2, sum) == repeated.sum.of.values ))
paste("Number of columns with the same exacts values:", length(rep.pos))
duplicated.expr.miR.tab <- expr.miR.tab[,rep.pos]

## Obtain the min value of each sample
## and the values of the repeated columns
min.sample.value <- sort(apply(expr.miR.tab, 1, min))
repeated.columns <- sort(duplicated.expr.miR.tab[,1])

## Get the miRNAs IDs that will be discarded
mir.ID.to.discard <- colnames(duplicated.expr.miR.tab)

## Compare the vectors, element by element.
## Sum the number of matches
nb.equal.values <- sum(min.sample.value == repeated.columns)

## The number of equal values should be the same as the number of samples
nb.samples <- nrow(expr.miR.tab)
# paste("Number of samples:", nb.samples, " - Number of equal values comparing min values and one duplicated column: ", nb.equal.values)

## Get the IDs of the selected miRs (with expr values)
selected.miRs.w.expr <- setdiff(colnames(expr.miR.tab), mir.ID.to.discard)


#######################################
## Update the miRNA expression table ##
#######################################
message("; Updating miR expression table")
## Update the dataframe with the selected miRs (miRs with low expression are discarded)
# bk <- expr.miR.tab
# expr.miR.tab <- expr.miR.tab[,-rep.pos]
expr.miR.tab <- expr.miR.tab[,selected.miRs.w.expr]

## Obtain the miR names in the expression table
miR.names.in.expr.tab <- colnames(expr.miR.tab)

## Iterate over the miR IDs in the expression table
## Identify those miR that are matching a pre-miR or whose ID is not matchin neither miR or premiR
IDs.match.premiR <- NULL
IDs.not.matched <- NULL
thrash <- sapply(miR.names.in.expr.tab, function(m){
  
  ## When the ID is matching a premiR, save it in the vector
  if(m %in% miR.premiR.tab$premiR){
    
    if(!(m %in% miR.premiR.tab$miR)){
      IDs.match.premiR <<- append(IDs.match.premiR, m)
    }
    
    ## Otherwise, check if it if not matching a miR ID
    ## if so, save the ID in a vector
  } else {
    if(!(m %in% miR.premiR.tab$miR)){
      IDs.not.matched <<- append(IDs.not.matched, m)
    }
  }
})


############################################################
## Manual curation of not-matched IDs in expression table ##
##
## Case 1: typo in the miR ID -> substitute
##
## Case 2: ID that was removed from miRbase -> remove
############################################################
message(";Curating wrongly annotated miR IDs")
not.matched.Ids.tab <- data.frame()
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-1233-1-5p", new_id = "hsa-mir-1233-5p", note = "Annotation error", action = "Replace"))
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-153", new_id = "hsa-mir-153-3p", note = "Annotation error", action = "Replace"))
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-219-5p", new_id = "hsa-mir-219a-5p", note = "Annotation error", action = "Replace"))
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-128", new_id = "hsa-mir-128-3p", note = "Revised", action = "Replace"))
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-133a", new_id = "hsa-mir-133a-3p", note = "Revised", action = "Replace"))
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-329", new_id = "hsa-mir-329-3p", note = "Revised", action = "Replace"))
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-1234-5p", new_id = NA, note = "Not found on databases", action = "Remove"))
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-3676-3p", new_id = NA, note = "Not found on databases: see comment http://www.mirbase.org/cgi-bin/mirna_entry.pl?acc=MI0016077", action = "Remove"))
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-3676-5p", new_id = NA, note = "Not found on databases: see comment http://www.mirbase.org/cgi-bin/mirna_entry.pl?acc=MI0016077", action = "Remove"))
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-5686", new_id = NA, note = "Not found on databases: see comment http://www.mirbase.org/cgi-bin/mirna_entry.pl?acc=MI0019290", action = "Remove"))
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-1273e", new_id = NA, note = "No premiR matching,To check: http://www.mirbase.org/cgi-bin/mirna_entry.pl?acc=MI0016059", action = "Remove"))
not.matched.Ids.tab <- rbind(not.matched.Ids.tab, data.frame(old_id = "hsa-mir-6724-5p", new_id = NA, note = "No premiR matching,To check: http://www.mirbase.org/cgi-bin/mirna_entry.pl?acc=MI0022559", action = "Remove"))

## Substitute or remove the ID according the 'Action' column
expr.tab.new.IDs <- colnames(expr.miR.tab)
ids.to.rm <- NULL
thrash <- sapply(1:nrow(not.matched.Ids.tab), function(l){
  
  action <- as.vector(not.matched.Ids.tab[l, "action"])
  old.id <- as.vector(not.matched.Ids.tab[l, "old_id"])
  old.id.ind <- which(expr.tab.new.IDs == old.id)
  
  ## Replace the ID
  if(action == "Replace"){
    
    ## Get the index and replace
    expr.tab.new.IDs[old.id.ind] <<- as.vector(not.matched.Ids.tab[l, "new_id"])
    
  } else if(action == "Remove"){
    
    ## Save the index
    ids.to.rm <<- append(ids.to.rm, old.id.ind)
  }
  
})
expr.miR.tab <- expr.miR.tab[,-ids.to.rm]
colnames(expr.miR.tab) <- expr.tab.new.IDs[-ids.to.rm]


#####################################################################
## Manual curation of IDs matching premiRs in the expression table ##
##
## The curation was done using the miRbase tracker tool:
## http://www.mirbasetracker.org/
##
## The IDs was substituted by the miR ID according the version
## 20 of miRbase
############################################################
IDs.matchin.premiRs.tab <- data.frame()
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-let-7c", new_id = "hsa-let-7c-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-1180", new_id = "hsa-mir-1180-3p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-1250", new_id = "hsa-mir-1250-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-1266", new_id = "hsa-mir-1266-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-1287", new_id = "hsa-mir-1287-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-1288", new_id = "hsa-mir-1288-3p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-1296", new_id = "hsa-mir-1296-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-1301", new_id = "hsa-mir-1301-3p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-134", new_id = "hsa-mir-134-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-1343", new_id = "hsa-mir-1343-3p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-152", new_id = "hsa-mir-152-3p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-181d", new_id = "hsa-mir-181d-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-190a", new_id = "hsa-mir-190a-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-1910", new_id = "hsa-mir-1910-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-210", new_id = "hsa-mir-210-3p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-215", new_id = "hsa-mir-215-5p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-2276", new_id = "hsa-mir-2276-3p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-3151", new_id = "hsa-mir-3151-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-328", new_id = "hsa-mir-328-3p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-370", new_id = "hsa-mir-370-3p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-372", new_id = "hsa-mir-372-3p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-383", new_id = "hsa-mir-383-5p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-410", new_id = "hsa-mir-410-3p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-433", new_id = "hsa-mir-433-3p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-487a", new_id = "hsa-mir-487a-3p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-487b", new_id = "hsa-mir-487b-3p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-489", new_id = "hsa-mir-489-3p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-494", new_id = "hsa-mir-494-3p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-504", new_id = "hsa-mir-504-5p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-5088", new_id = "hsa-mir-5088-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-510", new_id = "hsa-mir-510-5p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-513b", new_id = "hsa-mir-513b-5p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-5189", new_id = "hsa-mir-5189-5p"))
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-519d", new_id = "hsa-mir-519d-3p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-520f", new_id = "hsa-mir-520f-3p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-520g", new_id = "hsa-mir-520g-3p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-585", new_id = "hsa-mir-585-3p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-598", new_id = "hsa-mir-598-3p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-605", new_id = "hsa-mir-605-5p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-627", new_id = "hsa-mir-627-5p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-656", new_id = "hsa-mir-656-3p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-668", new_id = "hsa-mir-668-3p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-670", new_id = "hsa-mir-670-5p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-874", new_id = "hsa-mir-874-3p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-887", new_id = "hsa-mir-887-3p")) 
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-889", new_id = "hsa-mir-889-3p"))  
IDs.matchin.premiRs.tab <- rbind(IDs.matchin.premiRs.tab, data.frame(old_id = "hsa-mir-95", new_id = "hsa-mir-95-3p"))

## Substitute the column names
expr.tab.new.IDs <- colnames(expr.miR.tab)
thrash <- sapply(1:nrow(IDs.matchin.premiRs.tab), function(l){
  
  new.id <- as.vector(IDs.matchin.premiRs.tab[l, 2])
  old.id <- as.vector(IDs.matchin.premiRs.tab[l, 1])
  old.id.ind <- which(expr.tab.new.IDs == old.id)
  
  ## Get the index and replace
  expr.tab.new.IDs[old.id.ind] <<- new.id
  
})
colnames(expr.miR.tab) <- expr.tab.new.IDs

## Check that all the Ids in the miR expression table are miR
sum(colnames(expr.miR.tab) %in% miR.premiR.tab$miR) == length(colnames(expr.miR.tab))

##################################################################
## Restrict expression data to those samples with mutation data ##
##
## NOTE: this step is critical for the results
##################################################################
nb.samples.exp.data <- sum(rownames(expr.miR.tab) %in% unique(mut.miR.tab$sample))
expr.miR.tab <- expr.miR.tab[rownames(expr.miR.tab) %in% unique(mut.miR.tab$sample), ]


###############################
## Update the mutation table ##
###############################
# mut.miR.tab2 <- mut.miR.tab
# mut.miR.tab <- mut.miR.tab2
# dim(mut.miR.tab2)
message("; Updating mutation table")

## Calculate the total number of mutations
total.nb.mutations <- nrow(unique.data.frame(mut.miR.tab))

mut.miR.tab$hgnc_symbol <- as.vector(mut.miR.tab$hgnc_symbol)

## Select those premiRs (in the mut table) that are annotated in the miR - premiR association table
## This step is done since the mapping of mutations on pri-/pre-miRNAs is based on the precursor.
## Hence, once a mutation is associated to a given precursor, it is associated to both mature miRNAs.
dim(mut.miR.tab)
mut.miR.tab <- mut.miR.tab[which(mut.miR.tab$hgnc_symbol %in% miR.premiR.tab$premiR),]
dim(mut.miR.tab)


## Merge the mutation table with the miR - premiR key
colnames(mut.miR.tab)[1] <- "premiR"
mut.miR.tab <- merge(mut.miR.tab, miR.premiR.tab, by = "premiR")
dim(mut.miR.tab)

## In the mutation table: the first column (premiR) will be substituted by the miR ID
mut.miR.tab <- mut.miR.tab[, c(6,2,3,4,5,6,1)]
colnames(mut.miR.tab)[1] <- "hgnc_symbol"
colnames(mut.miR.tab)[6] <- "miR"
# dim(mut.miR.tab)

## Select those miRs (in the mut table) with expression data
mut.miR.tab <- mut.miR.tab[which(mut.miR.tab$hgnc_symbol %in% colnames(expr.miR.tab)), ]
# dim(mut.miR.tab)

## Remove repeated lines
# mut.miR.tab <- unique.data.frame(mut.miR.tab)

## Calculate the number of mutations per feature (primiR, premiR, promoters)
nb.mutations.per.feature <- mut.miR.tab %>% 
  group_by(premiR) %>% 
  tally()
colnames(nb.mutations.per.feature) <- c("premiR", "nb_mut")

nb.mutations.per.feature <- merge(nb.mutations.per.feature, miR.premiR.tab, by = "premiR")[,1:3]


##########################################
## Update the BED file with annotations ##
##########################################
# feature.bed2 <- feature.bed
# feature.bed <- feature.bed2

## Merge the BED file with the mutation info table
feature.bed <- merge(feature.bed, nb.mutations.per.feature, by = "premiR")[,c(8,2,3,4,1,6,7,9)]

## Merge the BED file with the information of the premir genomic region
feature.bed <- merge(feature.bed, premir.region.tab[,1:4], by = "premiR")

## Remove the column 'width rank'
feature.bed <- feature.bed[,c(1:6,8:11)]
# dim(feature.bed)

## Remove the repeated lines
feature.bed <- unique.data.frame(feature.bed)
# dim(feature.bed)

## The miR IDs in the BED must be those with expression data
feature.bed <- feature.bed[feature.bed$miR %in% colnames(expr.miR.tab), ]


#################################################
## Create table with data to run wilcoxon test ##
#################################################
message("; Computing U-test")
mut.tab.u.test <- NULL
mut.tab.u.test <- data.frame(expr = as.vector(expr.miR.tab))
mut.tab.u.test$miR <- rep(colnames(expr.miR.tab), each = nrow(expr.miR.tab))
mut.tab.u.test$samples <- rownames(expr.miR.tab)
in.exp <- paste(mut.tab.u.test$miR, mut.tab.u.test$samples, sep = "_")
in.mut <- paste(mut.miR.tab$hgnc_symbol, mut.miR.tab$sample, sep = "_")
mut.tab.u.test$mutated <- in.exp %in% in.mut

###########################
## Compute wilcoxon test ##
###########################
mut.miR.u.test <- sapply(unique(mut.tab.u.test$miR), function(m){
  
  ## Get the expression values of the miRs with and without mutations
  sample.w.mut <- subset(mut.tab.u.test, miR == m & mutated == TRUE)$expr
  sample.w.o.mut <- subset(mut.tab.u.test, miR == m & mutated == FALSE)$expr
  
  if(length(sample.w.mut) == 0 | length(sample.w.o.mut) == 0){
    pval.u.test <- "NA"
  } else {
    
    ## U-test calculation
    miRs.u.test <- wilcox.test(x = sample.w.mut,
                               y = sample.w.o.mut,
                               alternative = "two.sided",
                               paired = FALSE,
                               exact=TRUE)
    pval.u.test <- miRs.u.test[[3]]
    # pval.u.test <- prettyNum(pval.u.test, digits = 3)
  }
  
  pval.u.test
})
mut.miR.u.test <- data.frame(miR = names(mut.miR.u.test), U_test_pval = mut.miR.u.test)
n.test <- (nrow(mut.miR.u.test) - sum(is.na(mut.miR.u.test$U_test_pval)))
rownames(mut.miR.u.test) <- NULL
mut.miR.u.test$U_test_pval <- as.numeric(as.vector(mut.miR.u.test$U_test_pval))
mut.miR.u.test$U_test_eval <- mut.miR.u.test$U_test_pval * n.test
mut.miR.u.test$U_test_sig <- -log10(mut.miR.u.test$U_test_eval)
mut.miR.u.test$U_test_qval <- p.adjust(mut.miR.u.test$U_test_pval, method = "fdr")


########################################################
## Export the tables to run xseq + create HTML report ##
########################################################

## Merge all the information in a single table
feature.info.tab <- NULL
feature.info.tab <- merge(feature.bed, mut.miR.u.test, by = "miR")

## Add the column with the MIMAT
feature.info.tab$MIMAT <- as.vector(sapply(feature.info.tab$miR, function(m){
  
  mimat <- subset(mimat.mir.key, miR == m)$MIMAT
  
  if(length(mimat) > 0){
    mimat
  } else {
    "-"
  }
}))


## Add column wheter the miR is in mirgeneDB
feature.info.tab$mirgeneDB <- ifelse(feature.info.tab$MIMAT %in% mimat.mirgendb$MIMAT,
                                     yes = "YES",
                                     no = "NO")

## Add column wheter the miR is in FANOTM5 robust set
feature.info.tab$FANTOM5 <- ifelse(feature.info.tab$miR %in% mimat.F5.robust$miR,
                                   yes = "YES",
                                   no = "NO")

## Add column wheter the miR is in miRbase robust set
feature.info.tab$miRbase <- ifelse(feature.info.tab$MIMAT %in% mimat.mirbase.robust$MIMAT,
                                   yes = "YES",
                                   no = "NO")


## Export the feature info table
feature.info.tab.file <- file.path(folders.to.create[["xseq_tables"]], paste(prefix.name, "feature_info_table.tab", sep = "_"))
message(";Exporting feature information table: ", feature.info.tab.file)
write.table(x = feature.info.tab, file = feature.info.tab.file, sep = "\t", quote = FALSE, col.names = TRUE, row.names = FALSE)


## Export the parsed mutation table
parsed.mut.miR.tab.file <- file.path(folders.to.create[["xseq_tables"]], paste(prefix.name, "mutation_table.tab", sep = "_"))
message(";Exporting updated mutation table: ", parsed.mut.miR.tab.file)
write.table(mut.miR.tab, file = parsed.mut.miR.tab.file, sep = "\t", quote = FALSE, col.names = TRUE, row.names = FALSE)


## Export the parsed expression table
parsed.expr.miR.tab.file <- file.path(folders.to.create[["xseq_tables"]], paste(prefix.name, "expression_table.tab", sep = "_"))
message(";Exporting updated expression table: ", parsed.expr.miR.tab.file)
write.table(expr.miR.tab, file = parsed.expr.miR.tab.file, sep = "\t", quote = FALSE, col.names = TRUE, row.names = TRUE)