#####################
## Load R packages ##
#####################
required.libraries <- c("xseq")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    library(lib, character.only=TRUE)
  }
}


###################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message(";Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


if (!exists("results.dir")) {
  stop("Missing mandatory argument (Results folder): results.dir ")
} else if (!exists("mut.miR.tab.file")) {
  stop("Missing mandatory argument (Mutation table): mut.miR.tab.file ")
} else if (!exists("expr.miR.tab.file")) {
  stop("Missing mandatory argument (miR expression table): expr.miR.tab.file  ")
} else if (!exists("prefix.name")){
  stop("Missing mandatory argument prefix): prefix.name  ")
} else if (!exists("mut.types")){
  stop("Missing mandatory argument mutation type): mut.types  ")
}


## Set non-mandatory variables (only if they were not declared from the command line)
if (!exists("plot.format")) {
  plot.format <- c("pdf", "png");
}
feature.general.info <- list()
xseq.pred.mut.scores <- list()
folders.to.create <- list()
mut.types <- unlist(strsplit(x = mut.types, '-'))


##########################
# pri-miR  data to test ##
##########################
# results.dir <- file.path("results/xseq")
# prefix.name <- "primiRNA"
# mut.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/primiRNA_mutation_table.tab"
# expr.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/primiRNA_expression_table.tab"
# mut.types <- "both"

# results.dir <- file.path("results/xseq")
# prefix.name <- "primiRNA"
# mut.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/primiRNA_mutation_table.tab"
# expr.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/primiRNA_expression_table.tab"
# mut.types <- "both"


# results.dir <- file.path("results/xseq")
# prefix.name <- "superEnhancer"
# mut.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/SuperEnhancer_test/super_enhancers_mutation_table.tab"
# expr.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/SuperEnhancer_test/super_enhancers_expression_table.tab"
# mut.types <- "both"

results.dir <- file.path(results.dir, prefix.name)
folders.to.create[["xseq_tables"]] <- file.path(results.dir, "xseq_tables")


##############################
## Load miR mutations table ##
##############################
message(";Reading input tables")
message(";Reading mutation table: ", mut.miR.tab.file)
mut.miR.tab <- read.csv(mut.miR.tab.file, sep = "\t", header = TRUE)
mut.miR.tab$sample <- as.character(mut.miR.tab$sample)


###############################
## Load miR expression table ##
###############################
message(";Reading miR expression mutation table: ", expr.miR.tab.file)
expr.miR.tab <- read.csv(expr.miR.tab.file, sep = "\t", header = TRUE)

## Remove the "X" the sample names - This is added by R
## The sample IDs should be the rownames
colnames(expr.miR.tab) <- as.vector(sapply(names(expr.miR.tab), function(x){ gsub("\\.", "-", x)}))
rownames(expr.miR.tab) <- as.character(rownames(expr.miR.tab))


##########################################
## Normalizing data (required) for xseq ##
##########################################
message(";xseq - Estimating gene expression")
## Compute whether a gene is expressed in the studied tumour type. 
## If the expression data are from microarray, there is not need to compute weights. 
weight <- EstimateExpression(expr.miR.tab,
                             show.plot = FALSE)


## Impute missing values
expr <- ImputeKnn(expr.miR.tab)

## Quantile-Normalization
## Check this link: https://en.wikipedia.org/wiki/Quantile_normalization
## expr.quantile <- QuantileNorm(expr)
## NOTE: not required because the miR expression table is already normalized
expr.quantile <- expr
# dim(expr.quantile)
# sum(duplicated(t(expr)))
# sum(duplicated(t(expr.quantile)))


######################
## Initialize model ##
######################
message(";xseq - Get expression distribution")
## Show distribution without copy number alterations
expr.dis.quantile <- GetExpressionDistribution(expr=expr.quantile)

# id <- weight[mut.miR.tab[, "hgnc_symbol"]] >= 0.80
# id <- id[!is.na(id)]
# id <- names(id)

ids <- names(weight[which(weight >= 0.7)])

## Select those miR with high expression
mut.filt <- mut.miR.tab[which(mut.miR.tab$hgnc_symbol %in% ids),]

# dim(mut.miR.tab)
# dim(mut.filt)


########################################################################
## Calculate the posterior probabilities according the mutation types ##
########################################################################
message(";xseq - Calculating posterior probabilities")
# mut.types <- c("both", "loss", "gain")
# mut.types <- c("both")
init <- list()
model.cis <- list()
model.cis.em <- list()
xseq.pred <- list()
for(m in mut.types){
  
  message(";xseq - Mutation type: ", m)
  
  ## Set xseq priors
  message(";xseq - Set priors")
  init[[m]] <- SetXseqPrior(expr.dis = expr.dis.quantile,
                            mut <- mut.filt,
                            mut.type <- m,
                            cis <- TRUE)
  
  constraint <- list(equal.fg=FALSE)
  
  ## Initiate model
  message(";xseq - Initiallizing model")
  model.cis[[m]] = InitXseqModel(mut = mut.filt,
                                 expr = expr.quantile,
                                 expr.dis = expr.dis.quantile,
                                 cpd = init[[m]]$cpd,
                                 cis = TRUE,
                                 prior = init[[m]]$prior,
                                 debug = FALSE)
  
  ## EM step
  message(";xseq - Learning parameters (EM)")
  model.cis.em[[m]] = LearnXseqParameter(model = model.cis[[m]],
                                         constraint = constraint,
                                         iter.max = 50,
                                         threshold = 1e-6,
                                         cis = TRUE,
                                         debug = FALSE, 
                                         show.plot = FALSE)

  
  ## Print table with posterior probabilities
  message(";xseq - Exporting table with posterior probabilities")
  xseq.pred[[m]] <- ConvertXseqOutput(model.cis.em[[m]]$posterior)
  colnames(xseq.pred[[m]]) <- c("sample", "miR", "prob_mut", "prob_gene")
}


## Concatenate results
xseq.tab <- data.frame()
## Concatenate the xseq tables
for(m in mut.types){
  
  ## Add a column with the mutation type
  xseq.pred[[m]]$mut_type <- m
  
  ## Concatenate in a new dataframe
  xseq.tab <- rbind(xseq.tab, xseq.pred[[m]])
  
}


## Export xseq results
xseq.tab.file <- file.path(folders.to.create[["xseq_tables"]], paste(prefix.name, "xseq_posterior_prob_table.tab", sep = "_"))
message(";Exporting xseq result table: ", xseq.tab.file)
write.table(xseq.tab, file = xseq.tab.file , sep = "\t", quote = FALSE, col.names = TRUE, row.names = FALSE)