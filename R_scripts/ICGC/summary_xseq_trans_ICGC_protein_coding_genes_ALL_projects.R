#####################
## Load R packages ##
#####################
required.libraries <- c("amap",
                        "colorspace",
                        "data.table",
                        "dplyr",
                        "ggplot2",
                        "ggthemes",
                        "gridExtra",
                        "ggpubr",
                        "ggrepel",
                        "rcartocolor",
                        "reshape2")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    suppressPackageStartupMessages(library(lib, character.only=TRUE))
  }
}


###################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message("; Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


#####################
## Mandatory input ##
#####################
if (!exists("xseq.trans.merged.file")) {
  stop("Missing mandatory argument (A table with all the xseq trans reults for all the projects): xseq.trans.merged.file ")
  
} else if (!exists("xseq.supp.mat.file")) {
  stop("Missing mandatory argument (An Rdata with the xseq supplementary material tables): xseq.supp.mat.file ")
  
} else if (!exists("results.dir")) {
  stop("Missing mandatory argument (Output directory): results.dir ")
  
} 


####################
## Load utilities ##
####################
# source("/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/R_scripts/Utilities/dysmiR_utilities.R")
source("/storage/mathelierarea/processed/jamondra/Projects/dysmir/R_scripts/Utilities/dysmiR_utilities.R")


## Debug
# results.dir <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Manuscript/Figures"
# project.name <- "BRCA"
# xseq.trans.merged.file <- "/run/user/280010/gvfs/sftp:host=biotin2.hpc.uio.no,user=jamondra/storage/mathelierarea/processed/jamondra/Projects/dysmir/ICGC/results_ICGC/Pancancer_analysis/protein_coding_genes_data/xseq/trans/tables/xseq_trans_table_all_analysis_all_cancer_projects_cat_pcg.tab"
# xseq.supp.mat.file <- "/run/user/280010/gvfs/sftp:host=biotin2.hpc.uio.no,user=jamondra/storage/mathelierarea/processed/jamondra/Projects/dysmir/ICGC/results_ICGC/global/other/xseq_supp_mat/Xseq_supp_material_tables.Rdata"


###########################
## Create results folder ##
###########################
message("; Creating result folders")
folders.to.create <- list()
folders.to.create[["plots"]] <- file.path(results.dir, "plots")
folders.to.create[["tables"]] <- file.path(results.dir, "tables")
folders.to.create[["RData"]] <- file.path(results.dir, "RData")
trash <- sapply(folders.to.create, dir.create, recursive = TRUE, showWarnings = FALSE)


#############################
## Load xseq supp material ##
#############################
message("; Loading xseq supplementary material tables")
## Note: the object is called: xseq.sup.mat.tab
load(xseq.supp.mat.file)


#####################
## Load xseq table ##
#####################
message("; Loading xseq table: ", xseq.trans.merged.file)
xseq.trans.merged <- fread(xseq.trans.merged.file, header = TRUE, sep = "\t")

## Remove header of the concat tables
xseq.trans.merged <- xseq.trans.merged[xseq.trans.merged$sample != "sample", c(1:6,17:20)]

## Cast this columns to numeric values
xseq.trans.merged$Thr <- as.numeric(xseq.trans.merged$Thr)
xseq.trans.merged$prob_gene <- as.numeric(xseq.trans.merged$prob_gene)
xseq.trans.merged$prob_mut <- as.numeric(xseq.trans.merged$prob_mut)

## Count samples per project
nb.samples.project <- xseq.trans.merged %>%
                        group_by(project_name) %>%
                        summarise(Nb_samples = n_distinct(sample))

## Filter the table applying the cancer project specific threshold
xseq.trans.merged <- xseq.trans.merged %>% 
                      group_by(project_name) %>% 
                      filter(prob_gene >= Thr)

## Parse columns
xseq.trans.merged$shape_symb <- 19
xseq.trans.merged$gene <- as.factor(xseq.trans.merged$gene)
xseq.trans.merged$project_name <- as.factor(xseq.trans.merged$project_name)
xseq.trans.merged$mut_effect <- as.factor(xseq.trans.merged$mut_effect)


#############################
## Count the mutated genes ##
#############################

## For each gene on each project:
## 1) Get the line with the highest mut_prob and the max gene_prob
xseq.trans.merged.filt <- xseq.trans.merged %>% 
                          group_by(gene, project_name) %>%
                          filter(prob_gene == max(prob_gene)) %>%
                          filter(prob_mut == max(prob_mut))

## Remove the sample column and repeated lines
xseq.trans.merged.filt <- within(xseq.trans.merged.filt, rm(sample))
xseq.trans.merged.filt <- unique.data.frame(xseq.trans.merged.filt)
# View(xseq.trans.merged.filt)


## Count in how many projects each gene was significant
freq.genes.diff.projects <- xseq.trans.merged.filt %>% 
                              group_by(gene) %>%
                              tally(sort = TRUE)
# View(freq.genes.diff.projects)
freq.genes.diff.projects <- 
freq.genes.diff.projects %>% 
  dplyr::filter(n > 1)

## Count number of predicted genes in each project
nb.predicted.genes.by.project <- xseq.trans.merged.filt %>% 
                                  group_by(project_name) %>%
                                  tally(sort = TRUE)
colnames(nb.predicted.genes.by.project)[2] <- "Nb_predicted_genes"


#################################
## Plot nb genes vs nb samples ##
#################################
nb.genes.nb.samples.by.project <- merge(nb.predicted.genes.by.project, nb.samples.project, by = "project_name")

nb.genes.nb.samples.by.project.plot <- ggplot(nb.genes.nb.samples.by.project, aes(x = Nb_samples, y = Nb_predicted_genes, color = project_name)) +
                                        geom_point() +
                                        theme_bw() +
                                        # scale_color_discrete_qualitative(palette = "Dark3") +
                                        scale_colour_manual(breaks = names(assign.colors("Cohorts")),
                                                            values = assign.colors("Cohorts")) +
                                        # xlim(c(0,110)) +
                                        # ylim(c(0,110)) +
                                        geom_label_repel(aes(label = project_name),
                                                         box.padding   = 0.35, 
                                                         point.padding = 0.5,
                                                         direction = "both",
                                                         force = 2,
                                                         nudge_x = -1,
                                                         nudge_y = 0.75,
                                                         segment.color = 'grey50') +
                                        labs(title = "Nb of predicted genes vs Cohort size", x = "Cohort size", y = "Nb predicted genes") +
                                        theme(legend.position="none")

## Export the plot as Rdata
nb.genes.nb.samples.by.project.plot.rdata <- file.path(folders.to.create[["RData"]], "xseq_trans_protein_coding_nb_predicted_genes_vs_nb_samples.RData")
save(nb.genes.nb.samples.by.project.plot, file = nb.genes.nb.samples.by.project.plot.rdata)

## Export the plot as PDF
nb.genes.nb.samples.by.project.plot.pdf <- file.path(folders.to.create[["plots"]], "xseq_trans_protein_coding_nb_predicted_genes_vs_nb_samples.pdf")
message("; Exporting xseq summary plot: ", nb.genes.nb.samples.by.project.plot.pdf)
ggsave(nb.genes.nb.samples.by.project.plot.pdf, device = "pdf", width = 7, height = 7)


#############################
## Hierarchical clustering ##
#############################

## Keep genes predicted in at least N projects
xseq.trans.merged.filt <- xseq.trans.merged.filt[xseq.trans.merged.filt$gene %in% freq.genes.diff.projects$gene,]


xseq.trans.merged.filt.matrix <- acast(melt(xseq.trans.merged.filt[,c("gene", "prob_gene", "project_name")]), gene ~ project_name, fill = 0)
xseq.trans.merged.filt.matrix <- as.matrix(xseq.trans.merged.filt.matrix)

## Clustered gene names
xseq.trans.merged.filt.matrix.d <- Dist(xseq.trans.merged.filt.matrix, method = "euclidean")
xseq.trans.merged.filt.matrix.hc <- hclust(xseq.trans.merged.filt.matrix.d, method = "ward.D")
xseq.trans.merged.filt.matrix.hc.genes.order <- xseq.trans.merged.filt.matrix.hc$labels[xseq.trans.merged.filt.matrix.hc$order]

## Clustered project names
xseq.trans.merged.filt.matrix.d <- Dist(t(xseq.trans.merged.filt.matrix), method = "euclidean")
xseq.trans.merged.filt.matrix.hc <- hclust(xseq.trans.merged.filt.matrix.d, method = "ward.D")
xseq.trans.merged.filt.matrix.hc.projects.order <- xseq.trans.merged.filt.matrix.hc$labels[xseq.trans.merged.filt.matrix.hc$order]


######################
## Draw the heatmap ##
######################
# heatmap.summary <- ggplot(xseq.trans.merged.filt, aes(x = gene, y = project_name, fill = prob_gene, height = prob_mut, width = prob_mut)) +
heatmap.summary <- ggplot(xseq.trans.merged.filt, aes(x = gene, y = project_name, fill = prob_gene)) +
                    geom_tile(width = 1, height = 0.975) +
                    # coord_fixed() + 
                    # scale_fill_continuous(low="#7fcdbb", high="#bd0026", guide="colorbar", na.value="white", limits=c(0.8, 1)) +
                    # scale_fill_distiller(palette = "PuBuGn", direction = +1, limits = c(0.8,1)) +
                    scale_fill_carto_c(palette = "Sunset", direction = +1,       ## Continue color scale
                     limits = c(0.8,1)) +
                    theme_classic() +
                    labs(title = "", x = "Gene Symbol", y = "Project") +
                    theme(axis.text.x = element_text(angle = 45, hjust = 1.1,size=5,colour="black",face="bold"), axis.text.y = element_text(size=5,colour="black"), axis.title.y = element_text(size=5,colour = "black",face="bold",vjust=0.12)) +
                    theme(legend.text = element_text(size = 5), legend.title=element_text(size=5)) +
                    theme(legend.direction = "vertical", legend.position = "left") +
                    theme(axis.text.y = element_text(color = ifelse(xseq.trans.merged.filt.matrix.hc.genes.order %in% xseq.sup.mat.tab[["Cancer_gene_list_intersection"]], 
                                                                    yes = "darkred",
                                                                    no = "darkgrey"))) +
                    # scale_x_discrete(limits = unlist(unique(xseq.trans.merged$gene))) +
                    # scale_y_discrete(limits = unlist(unique(xseq.trans.merged$project))) +
                    scale_x_discrete(limits = xseq.trans.merged.filt.matrix.hc.genes.order) +
                    scale_y_discrete(limits = xseq.trans.merged.filt.matrix.hc.projects.order) +
                    coord_flip()


## Export the plot as Rdata
heatmap.summary.rdata <- file.path(folders.to.create[["RData"]], "xseq_trans_protein_coding_results_summary_plot.pdf")
save(heatmap.summary, file = heatmap.summary.rdata)

## Export the plot as PDF
xseq.results.summary.plot <- file.path(folders.to.create[["plots"]], "xseq_trans_protein_coding_results_summary_plot.pdf")
message("; Exporting xseq summary plot: ", xseq.results.summary.plot)
ggsave(xseq.results.summary.plot, device = "pdf", width = 7, height = 20)
# ggsave(xseq.results.summary.plot, device = "pdf", width = 20, height = 5)
# ggsave(xseq.results.summary.plot, device = "pdf", limitsize = FALSE, width = 35, height = 10)

##############################################
## Export table with results and            ##
## comparison versus supplementary material ##
##############################################
projects <- unique(as.vector(xseq.trans.merged.filt$project_name))
xseq.trans.merged.filt.annotated <- data.frame()
for(p in projects){
  
  ## Select the lines corresponding to a given project
  xseq.selection <- subset(xseq.trans.merged.filt, project_name == p)
  
  ## Selected genes of a given project
  selected.genes <- as.vector(xseq.selection$gene)
  
  ## Remove the suffix '-US' from the project name
  ## This is required since project names in xseq results do not contain the suffix.
  p.parsed <- gsub(pattern = "-US", replacement = "", x = p)
  
  ## Check if the predicted genes (in this analysis) were reported in the original xseq publication
  ## 1) In trans analysis
  ## 2) As Bona fide cancer genes
  ## 3) As unknown cancer status genes
  ## 4) As false positive cancer genes
  selection.xseq.supp.mat.tumour.type <- subset(xseq.sup.mat.tab[["pred_genes_trans"]], Tumour_type == p.parsed)
  predictions.reported.trans <- selected.genes %in% selection.xseq.supp.mat.tumour.type$HGNC_symbol
  # predictions.reported.bonafide <- selected.genes %in% xseq.sup.mat.tab[["bona_fide_cancer_genes"]]$HGNC_symbol
  predictions.reported.bonafide <- selected.genes %in% xseq.sup.mat.tab[["cancer_gene_census"]]
  predictions.reported.unknown.cancer.status <- selected.genes %in% xseq.sup.mat.tab[["unknown_cancer_status_genes"]]
  predictions.reported.false.positive.cancer <- selected.genes %in% xseq.sup.mat.tab[["cancer_genes_false_positive"]]
  
  ## Calculate the percentage of genes reported as significant in this analysis
  ## relative to the number of reported genes in the xseq publication
  prediction.percentage <- sum(selection.xseq.supp.mat.tumour.type$HGNC_symbol %in% selected.genes) / length(selected.genes)
  prediction.percentage <- round(prediction.percentage, digits = 2)
    
  ## Check if the predicted genes are reported in the Cancer Gene Census
  selected.genes.cgc <- selected.genes %in% xseq.sup.mat.tab[["cancer_gene_census"]]
  selected.genes.cgc.perc <- round(sum(selected.genes.cgc)/length(selected.genes), digits = 2)
    
  ## Check if the predicted genes are reported in the Network Cancer Gene
  selected.genes.ncg <- selected.genes %in% xseq.sup.mat.tab[["network_cancer_gene"]]
  selected.genes.ncg.perc <- round(sum(selected.genes.ncg)/length(selected.genes), digits = 2)
  
  ## Check if the predicted genes are reported in inToGen
  selected.genes.intogen <- selected.genes %in% xseq.sup.mat.tab[["inToGen_names"]]
  selected.genes.intogen.perc <- round(sum(selected.genes.intogen)/length(selected.genes), digits = 2)
  
  ## Check if the predicted genes are reported in the combined cancer driver gene list
  selected.genes.driver.list.int <- selected.genes %in% xseq.sup.mat.tab[["Cancer_gene_list_intersection"]]
  selected.genes.driver.list.int.perc <- round(sum(selected.genes.driver.list.int)/length(selected.genes), digits = 2)
  

  ## Add the columns
  xseq.selection$Supp_mat_trans <- predictions.reported.trans
  xseq.selection$Supp_mat_trans_overlap <- rep(prediction.percentage, times = nrow(xseq.selection))
  xseq.selection$Bona_fide <- predictions.reported.bonafide
  xseq.selection$Supp_mat_unknown_cancer_status <- predictions.reported.unknown.cancer.status
  xseq.selection$Supp_mat_false_positive_cancer_gene <- predictions.reported.false.positive.cancer
  xseq.selection$Cancer_Gene_Census <- selected.genes.cgc
  xseq.selection$Cancer_Gene_Census_ovrlp <- selected.genes.cgc.perc
  xseq.selection$Network_Cancer_Gene <- selected.genes.ncg
  xseq.selection$Network_Cancer_Gene_ovrlp <- selected.genes.ncg.perc
  xseq.selection$inToGen <- selected.genes.intogen
  xseq.selection$inToGen_ovrlp <- selected.genes.intogen.perc
  xseq.selection$CancerDriverInt <- selected.genes.driver.list.int
  xseq.selection$CancerDriverInt_ovrlp <- selected.genes.driver.list.int.perc
  xseq.selection <- as.data.frame(xseq.selection)
  
  ## Concatenate the results
  xseq.trans.merged.filt.annotated <- rbind(xseq.trans.merged.filt.annotated, xseq.selection)
}

## Export the table
xseq.results.annotated.file <- file.path(folders.to.create[["tables"]], "xseq_trans_protein_coding_results_with_annotations.tab")
message("; Exporting xseq prediction with annotations: ", xseq.results.annotated.file)
write.table(xseq.trans.merged.filt.annotated, file = xseq.results.annotated.file, sep = "\t", quote = FALSE, col.names = TRUE, row.names = FALSE)
