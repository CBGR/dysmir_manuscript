#####################
## Load R packages ##
#####################
required.libraries <- c("edgeR",
                        "dplyr",
                        "preprocessCore",
                        "reshape2",
                        "xseq")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    suppressPackageStartupMessages(library(lib, character.only=TRUE))
  }
}


###################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message(";Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


if (!exists("results.dir")) {
  stop("Missing mandatory argument (Results folder): results.dir ")
} else if (!exists("mut.miR.tab.file")) {
  stop("Missing mandatory argument (Mutation table): mut.miR.tab.file ")
} else if (!exists("expr.miR.tab.file")) {
  stop("Missing mandatory argument (miR expression table): expr.miR.tab.file  ")
} else if (!exists("miR.premiR.tab.file")) {
  stop("Missing mandatory argument (miR <-> pre-miR association table): miR.premiR.tab.file  ")
} else if (!exists("prefix.name")){
  stop("Missing mandatory argument prefix): prefix.name  ")
} else if (!exists("feature.bed.file")){
  stop("Missing mandatory argument (feature BED file)): feature.bed.file")
} else if (!exists("mimat.mir.key.file")){
  stop("Missing mandatory argument (MIMAT <-> miR association table)): mimat.mir.key.file")
} else if (!exists("mimat.mirgendb.file")){
  stop("Missing mandatory argument (MIMAT in mirgendb table): mimat.mirgendb.file")
} else if (!exists("mimat.F5.robust.file")){
  stop("Missing mandatory argument (MIMAT robust mir FANTOM5 table): mimat.F5.robust.file")
} else if (!exists("mimat.mirbase.robust.file")){
  stop("Missing mandatory argument (MIMAT robust mir mirbase table): mimat.mirbase.robust.file")
} else if (!exists("premir.region.tab.file")){
  stop("Missing mandatory argument (Genomic region of pre-miRNAs (intergenic/intronic) ): premir.region.tab.file")
} 


##########################
# pri-miR  data to test ##
##########################
# results.dir <- file.path("results/xseq_ICGC")
# prefix.name <- "primiRNA"
# mut.miR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/ICGC_mutations_in_primiRs.tsv"
# miR.premiR.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/hsa_mir_to_premir.tsv"
# feature.bed.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/primiRs_F5.bed"
# mimat.mir.key.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/key_MIMAT_miRNA.txt"
# mimat.mirgendb.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/mirgeneDB_MIMATs.tab"
# mimat.F5.robust.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/FANTOM5_robust_miRs.tab"
# mimat.mirbase.robust.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/mirbase_robust_MIMATs.tab"
# premir.region.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/premiR_genomic_region.tab"
# expr.miR.tab.file <-"/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/ICGC_miR_expression_table.tab" ## Raw counts


###########################
## Create results folder ##
###########################
message(";Creating result folders")
results.dir <- file.path(results.dir, prefix.name)
folders.to.create <- list()
folders.to.create[["prefix"]] <- results.dir
folders.to.create[["plots"]] <- file.path(results.dir, "plots")
folders.to.create[["xseq_tables"]] <- file.path(results.dir, "xseq_tables")
thrash <- sapply(folders.to.create, dir.create, recursive = TRUE, showWarnings = FALSE)


##############################
## Load miR mutations table ##
##############################
message(";Reading input tables")
message(";Reading mutation table: ", mut.miR.tab.file)
mut.miR.tab <- read.csv(mut.miR.tab.file, sep = "\t", header = FALSE)
colnames(mut.miR.tab) <- c("sample", "hgnc_symbol", "variant_type", "chr", "pos", "variant", "transcript_id", "gene_id")
mut.miR.tab$hgnc_symbol <- as.vector(sapply(mut.miR.tab$hgnc_symbol, function(x){ gsub("_", "-", x)}))
mut.miR.tab$hgnc_symbol <- tolower(mut.miR.tab$hgnc_symbol)
# mut.miR.tab$sample <- as.character(mut.miR.tab$sample)
# View(head(mut.miR.tab))


###############################
## Load miR expression table ##
###############################
message(";Reading miR expression mutation table: ", expr.miR.tab.file)
expr.miR.tab <- read.csv(expr.miR.tab.file, sep = "\t", header = TRUE)

## Remove the "X" the sample names - This is added by R
## The sample IDs should be the rownames
# rownames(expr.miR.tab) <- as.vector(sapply(rownames(expr.miR.tab), function(x){ gsub("-", "_", x)}))
colnames(expr.miR.tab) <- as.vector(sapply(colnames(expr.miR.tab), function(x){ gsub("\\.", "-", x)}))

## Transpose the table - This is required for the xseq input
## Columns (after transposition): miRNA ID
## Rows (after transposition): sample ID
colnames(expr.miR.tab) <- tolower(colnames(expr.miR.tab))
# rownames(expr.miR.tab) <- tolower(rownames(expr.miR.tab))

## The matrix must be transposed to apply the cpm
expr.miR.tab <- t(expr.miR.tab)

expr.miR.tab <- DGEList(expr.miR.tab)

expr.miR.tab <- calcNormFactors(expr.miR.tab,
                                method = "TMM",
                                doWeighting = TRUE)

# expr.miR.tab <- calcNormFactors(expr.miR.tab,
#                                 method = "upperquartile",
#                                 p = 0.9)


## Obtain the CPM matrix
## The filtering must be donde after filtering
expr.miR.tab <- cpm(expr.miR.tab)

## Remove duplicated
expr.miR.tab <- expr.miR.tab[!duplicated(expr.miR.tab), ]

## Keep the miRs with at least 5 reads in at least 5 patients
expr.miR.tab <- expr.miR.tab[ rowSums(expr.miR.tab >= 5) >= 10 , ]


## log2 transformation
expr.miR.tab <- log2(expr.miR.tab + 0.5)

# expr.miR.tab[1:5, 1:5]

# expr.miR.tab <- expr.miR.tab[round(apply(expr.miR.tab, 1, median) ) >= 1, ]

# Quantile-normalization
# expr.miR.tab <- as.matrix(expr.miR.tab, rownames.force = TRUE)
# expr.miR.tab.n <- normalize.quantiles(expr.miR.tab)
# colnames(expr.miR.tab.n) <- colnames(expr.miR.tab)
# rownames(expr.miR.tab.n) <- rownames(expr.miR.tab)
# expr.miR.tab <- expr.miR.tab.n


expr.miR.tab <- t(expr.miR.tab)

## Export the parsed expression table
parsed.expr.miR.tab.file <- file.path(folders.to.create[["xseq_tables"]], paste(prefix.name, "expression_table.tab", sep = "_"))
message(";Exporting updated expression table: ", parsed.expr.miR.tab.file)
write.table(expr.miR.tab, file = parsed.expr.miR.tab.file, sep = "\t", quote = FALSE, col.names = TRUE, row.names = TRUE)


# hist(expr.miR.tab,
#      breaks = 20)


###################
## Load BED file ##
###################
message(";Reading BED file: ", feature.bed.file)
feature.bed <- read.csv(feature.bed.file, sep = "\t", header = FALSE)

## Get the columns with the miR name
mir.col <- as.vector(sapply(feature.bed[1,], function(x){grepl("hsa-", x)}))
mir.col <- mir.col * 1:length(as.vector(feature.bed[1,]))
mir.col <- as.vector(which(mir.col != 0))
feature.bed <- feature.bed[,c(1:3, mir.col)]
colnames(feature.bed) <- c("chr", "start", "end", "premiR")
feature.bed$premiR_ID <- tolower(feature.bed$premiR)
feature.bed <- mutate(feature.bed, feature_width = end - start)
feature.bed <- mutate(feature.bed , width_rank = rank(feature.bed$feature_width))


##########################################
## Load miR - pre-miR association table ##
##########################################
message(";Reading miR-premiR association table: ", miR.premiR.tab.file)
miR.premiR.tab <- read.csv(miR.premiR.tab.file, sep = "\t", header = FALSE)
colnames(miR.premiR.tab) <- c("miR", "premiR")

miR.premiR.tab$miR <- tolower(miR.premiR.tab$miR)
miR.premiR.tab$premiR <- tolower(miR.premiR.tab$premiR)


## Add a new column with the number of times each miR is found on different pre-miR
miR.premiR.tab$nb_miR_dif_loci <- sapply(miR.premiR.tab$miR, function(m){
  length(which(miR.premiR.tab$miR == m))
})

## Add a new column with the premiRs associated to each miR
miR.premiR.tab$premiRs_associated_to_miR <- sapply(miR.premiR.tab$miR, function(m){
  selected.premiR <- as.vector(subset(miR.premiR.tab, miR == m)[,2])
  selected.premiR.print <- paste(selected.premiR, collapse = ",")
})

## Add a new column with the number of miRs within the selected pre-miR
miR.premiR.tab$nb_miR_in_premiR <- sapply(miR.premiR.tab$premiR, function(p){
  length(which(miR.premiR.tab$premiR == p))
})

## Add a new column with the names of the miRs within the selected pre-miR
miR.premiR.tab$miR_in_premiR <- sapply(miR.premiR.tab$premiR, function(p){
  selected.miR <- as.vector(subset(miR.premiR.tab, premiR == p)[,1])
  selected.miR.print <- paste(selected.miR, collapse = ",")
})

## Create a list with the miRNA (content) - pre-miRNA (names) association
miR.to.premiR <- miR.premiR.tab$miR
names(miR.to.premiR) <- miR.premiR.tab$premiR

## Create a list with the pre-miRNA (content) - miRNA (names) association
premiR.to.miR <- miR.premiR.tab$premiR
names(premiR.to.miR) <- miR.premiR.tab$miR


##############################################
## Load the genomic region of premiRs table ##
##############################################
message(";Load the genomic region of premiRs table: ", premir.region.tab.file)
premir.region.tab <- read.csv(premir.region.tab.file, sep = "\t", header = TRUE)
premir.region.tab <- premir.region.tab[,c(1,2,4)]
colnames(premir.region.tab) <- c("promoter", "region", "premiR")
premir.region.tab$premiR <- tolower(premir.region.tab$premiR)

## Obtain the name of the host gene
## The name is parsed from the promoter column when the premiR is intronic and its name does not contain genomic coordinates
premir.region.tab$host_gene <- ifelse(premir.region.tab$region == "intronic" & !grepl("chr\\d+", premir.region.tab$promoter, perl = TRUE),
                                      yes = gsub(premir.region.tab$promoter, pattern = "p\\d+@", replacement = "", perl = TRUE),
                                      no = NA)

## Merge the premiR genomic region with the miR-premiR table
premir.region.tab <- merge(premir.region.tab, miR.premiR.tab, by = "premiR")[,1:5]


##########################################
## Load MIMAT <-> miR association table ##
##########################################
message(";Reading MIMAT <-> miR association table: ", mimat.mir.key.file)
mimat.mir.key <- read.csv(mimat.mir.key.file, sep = "\t", header = TRUE)
colnames(mimat.mir.key) <- c("MIMAT", "miR")
mimat.mir.key$MIMAT <- tolower(mimat.mir.key$MIMAT)
mimat.mir.key$miR <- tolower(mimat.mir.key$miR)


######################################
## Load MIMAT in FANTOM5 robust set ##
######################################
message(";Reading MIMAT in FANTOM5 robust set: ", mimat.F5.robust.file)
mimat.F5.robust <- read.csv(mimat.F5.robust.file, sep = "\t", header = FALSE)
colnames(mimat.F5.robust) <- c("miR", "status")
mimat.F5.robust$miR <- tolower(mimat.F5.robust$miR)


######################################
## Load MIMAT in miRbase robust set ##
######################################
message(";Reading MIMAT in miRbase robust set: ", mimat.mirbase.robust.file)
mimat.mirbase.robust <- read.csv(mimat.mirbase.robust.file, sep = "\t", header = FALSE)
colnames(mimat.mirbase.robust) <- c("MIMAT")
mimat.mirbase.robust$MIMAT <- tolower(mimat.mirbase.robust$MIMAT)


############################
## Load MIMAT in mirgenDB ##
############################
message(";Reading MIMAT in mirgenDB robust set: ", mimat.mirgendb.file)
mimat.mirgendb <- read.csv(mimat.mirgendb.file, sep = "\t", header = FALSE)
colnames(mimat.mirgendb) <- c("MIMAT")
mimat.mirgendb$MIMAT <- tolower(mimat.mirgendb$MIMAT)


###########################################################
## Update the mutation table 
##
## Select the mutations for premiRs with expression data
###########################################################
# mut.miR.tab2 <- mut.miR.tab
# mut.miR.tab <- mut.miR.tab2
# dim(mut.miR.tab2)
# message(";Updating mutation table")

## Calculate the total number of mutations
total.nb.mutations <- nrow(unique.data.frame(mut.miR.tab))

mut.miR.tab$hgnc_symbol <- as.vector(mut.miR.tab$hgnc_symbol)

## Calculate the number of mutations per feature (primiR, premiR, promoters)
nb.mutations.per.feature <- mut.miR.tab %>% 
  group_by(hgnc_symbol) %>% 
  tally()
colnames(nb.mutations.per.feature) <- c("premiR", "nb_mut")

nb.mutations.per.feature <- merge(nb.mutations.per.feature, miR.premiR.tab, by = "premiR")[,1:3]


##################################################################
## Restrict expression data to those samples with mutation data ##
##
## NOTE: this step is critical for the results
##################################################################
nb.samples.exp.data <- sum(rownames(expr.miR.tab) %in% unique(mut.miR.tab$sample))
# expr.miR.tab <- expr.miR.tab[rownames(expr.miR.tab) %in% unique(mut.miR.tab$sample), ]


##########################################
## Update the BED file with annotations ##
##########################################
# feature.bed2 <- feature.bed
# feature.bed <- feature.bed2

## Merge the BED file with the mutation info table
feature.bed <- merge(feature.bed, nb.mutations.per.feature, by = "premiR")[,c(1,2,3,4,6,8,9)]

## Merge the BED file with the information of the premir genomic region
feature.bed <- merge(feature.bed, premir.region.tab[,1:4], by = "premiR")

## Remove the repeated lines
# dim(feature.bed)
feature.bed <- unique.data.frame(feature.bed)
# dim(feature.bed)

## The miR IDs in the BED must be those with expression data
feature.bed <- feature.bed[feature.bed$premiR %in% colnames(expr.miR.tab), ]


#################################################
## Create table with data to run wilcoxon test ##
#################################################
message("; Computing U-test")
mut.tab.u.test <- NULL
mut.tab.u.test <- data.frame(expr = as.vector(expr.miR.tab))
mut.tab.u.test$premiR <- rep(colnames(expr.miR.tab), each = nrow(expr.miR.tab))
mut.tab.u.test$samples <- rownames(expr.miR.tab)
in.exp <- paste(mut.tab.u.test$premiR, mut.tab.u.test$samples, sep = "_")
in.mut <- paste(mut.miR.tab$hgnc_symbol, mut.miR.tab$sample, sep = "_")
mut.tab.u.test$mutated <- in.exp %in% in.mut

###########################
## Compute wilcoxon test ##
###########################
mut.miR.u.test <- sapply(unique(mut.tab.u.test$premiR), function(m){
  
  ## Get the expression values of the miRs with and without mutations
  sample.w.mut <- subset(mut.tab.u.test, premiR == m & mutated == TRUE)$expr
  sample.w.o.mut <- subset(mut.tab.u.test, premiR == m & mutated == FALSE)$expr
  
  if(length(sample.w.mut) == 0 | length(sample.w.o.mut) == 0){
    pval.u.test <- "NA"
  } else {
    
    ## U-test calculation
    miRs.u.test <- wilcox.test(x = sample.w.mut,
                               y = sample.w.o.mut,
                               alternative = "two.sided",
                               paired = FALSE,
                               exact=TRUE)
    pval.u.test <- miRs.u.test[[3]]
    # pval.u.test <- prettyNum(pval.u.test, digits = 3)
  }
  
  pval.u.test
})
mut.miR.u.test <- data.frame(premiR = names(mut.miR.u.test), U_test_pval = mut.miR.u.test)
n.test <- (nrow(mut.miR.u.test) - sum(is.na(mut.miR.u.test$U_test_pval)))
rownames(mut.miR.u.test) <- NULL
mut.miR.u.test$U_test_pval <- as.numeric(as.vector(mut.miR.u.test$U_test_pval))
mut.miR.u.test$U_test_eval <- mut.miR.u.test$U_test_pval * n.test
mut.miR.u.test$U_test_sig <- -log10(mut.miR.u.test$U_test_eval)
mut.miR.u.test$U_test_qval <- p.adjust(mut.miR.u.test$U_test_pval, method = "fdr")


########################################################
## Export the tables to run xseq + create HTML report ##
########################################################

## Merge all the information in a single table
feature.info.tab <- NULL
feature.info.tab <- merge(feature.bed, mut.miR.u.test, by = "premiR")

## Add the column with the MIMAT
feature.info.tab$MIMAT <- as.vector(sapply(feature.info.tab$miR, function(m){
  
  mimat <- subset(mimat.mir.key, miR == m)$MIMAT
  
  if(length(mimat) > 0){
    mimat
  } else {
    "-"
  }
}))


## Add column wheter the miR is in mirgeneDB
feature.info.tab$mirgeneDB <- ifelse(feature.info.tab$MIMAT %in% mimat.mirgendb$MIMAT,
                                     yes = "YES",
                                     no = "NO")

## Add column wheter the miR is in FANOTM5 robust set
feature.info.tab$FANTOM5 <- ifelse(feature.info.tab$miR %in% mimat.F5.robust$miR,
                                   yes = "YES",
                                   no = "NO")

## Add column wheter the miR is in miRbase robust set
feature.info.tab$miRbase <- ifelse(feature.info.tab$MIMAT %in% mimat.mirbase.robust$MIMAT,
                                   yes = "YES",
                                   no = "NO")


## Export the feature info table
feature.info.tab.file <- file.path(folders.to.create[["xseq_tables"]], paste(prefix.name, "feature_info_table.tab", sep = "_"))
message(";Exporting feature information table: ", feature.info.tab.file)
write.table(x = feature.info.tab, file = feature.info.tab.file, sep = "\t", quote = FALSE, col.names = TRUE, row.names = FALSE)



##########################################
## Normalizing data (required) for xseq ##
##########################################
message(";xseq - Estimating gene expression")
## Compute whether a gene is expressed in the studied tumour type. 
## If the expression data are from microarray, there is not need to compute weights. 
weight <- EstimateExpression(expr.miR.tab,
                             show.plot = TRUE)
## Impute missing values
expr <- ImputeKnn(expr.miR.tab)

## Quantile-Normalization
## Check this link: https://en.wikipedia.org/wiki/Quantile_normalization
message(";xseq - Quantile Normalization")
expr.quantile <- QuantileNorm(expr)

## NOTE: not required because the miR expression table is already normalized
# expr.quantile <- expr


######################
## Initialize model ##
######################
message(";xseq - Get expression distribution")
## Show distribution without copy number alterations
expr.dis.quantile <- GetExpressionDistribution(expr=expr.quantile)

# x <- GetExpressionDistribution(expr=expr.quantile,gene = "hsa-mir-675", show.plot = TRUE)
# x <- GetExpressionDistribution(expr=expr.quantile,gene = "hsa-mir-654", show.plot = TRUE)
# x <- GetExpressionDistribution(expr=expr.quantile,gene = "hsa-mir-409", show.plot = TRUE)
# x <- GetExpressionDistribution(expr=expr.quantile,gene = "hsa-mir-379", show.plot = TRUE)
# x <- GetExpressionDistribution(expr=expr.quantile,gene = "hsa-mir-127", show.plot = TRUE)

# id <- weight[mut.miR.tab[, "hgnc_symbol"]] >= 0.80
# id <- id[!is.na(id)]
# id <- names(id)

ids <- names(weight[which(weight >= 0.7)])

## Select those miR with high expression
mut.filt <- mut.miR.tab[mut.miR.tab$hgnc_symbol %in% ids,]

# dim(mut.miR.tab)
# dim(mut.filt)


########################################################################
## Calculate the posterior probabilities according the mutation types ##
########################################################################
message(";xseq - Calculating posterior probabilities")
# mut.types <- c("both", "loss", "gain")
mut.types <- c("both")
init <- list()
model.cis <- list()
model.cis.em <- list()
xseq.pred <- list()
for(m in mut.types){
  
  message(";xseq - Mutation type: ", m)
  
  ## Set xseq priors
  message(";xseq - Set priors")
  init[[m]] <- SetXseqPrior(expr.dis = expr.dis.quantile,
                            mut <- mut.filt,
                            mut.type <- m,
                            cis <- TRUE)
  
  constraint <- list(equal.fg=FALSE)
  
  ## Initiate model
  message(";xseq - Initiallizing model")
  model.cis[[m]] = InitXseqModel(mut = mut.filt,
                                 expr = expr.quantile,
                                 expr.dis = expr.dis.quantile,
                                 cpd = init[[m]]$cpd,
                                 cis = TRUE,
                                 prior = init[[m]]$prior,
                                 debug = FALSE)
  
  ## EM step
  message(";xseq - Learning parameters (EM)")
  model.cis.em[[m]] = LearnXseqParameter(model = model.cis[[m]],
                                         constraint = constraint,
                                         iter.max = 50,
                                         threshold = 1e-6,
                                         cis = TRUE,
                                         debug = FALSE, 
                                         show.plot = FALSE)
  
  
  ## Print table with posterior probabilities
  message(";xseq - Exporting table with posterior probabilities")
  xseq.pred[[m]] <- ConvertXseqOutput(model.cis.em[[m]]$posterior)
  colnames(xseq.pred[[m]]) <- c("sample", "miR", "prob_mut", "prob_gene")
}
# View(xseq.pred[[m]])

## Concatenate results
xseq.tab <- data.frame()
## Concatenate the xseq tables
for(m in mut.types){
  
  ## Add a column with the mutation type
  xseq.pred[[m]]$mut_type <- m
  
  ## Concatenate in a new dataframe
  xseq.tab <- rbind(xseq.tab, xseq.pred[[m]])
  
}


## Export xseq results
xseq.tab.file <- file.path(folders.to.create[["xseq_tables"]], paste(prefix.name, "xseq_posterior_prob_table.tab", sep = "_"))
message(";Exporting xseq result table: ", xseq.tab.file)
write.table(xseq.tab, file = xseq.tab.file , sep = "\t", quote = FALSE, col.names = TRUE, row.names = FALSE)
