#####################
## Load R packages ##
#####################
required.libraries <- c("data.table",
                        "dplyr",
                        "ggplot2",
                        "ggthemes",
                        "gridExtra",
                        "ggpubr",
                        "reshape2")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    suppressPackageStartupMessages(library(lib, character.only=TRUE))
  }
}

###################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message("; Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


#####################
## Mandatory input ##
#####################
if (!exists("xseq.cis.merged.file")) {
  stop("Missing mandatory argument (A table with all the xseq cis reults for all the projects): xseq.cis.merged.file ")
  
} else if (!exists("xseq.supp.mat.file")) {
  stop("Missing mandatory argument (An Rdata with the xseq supplementary material tables): xseq.supp.mat.file ")
  
} else if (!exists("results.dir")) {
  stop("Missing mandatory argument (Output directory): results.dir ")
  
} 


## Debug
# xseq.cis.merged.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/other/dysmiR_revised_version_input_20-08-2018/xseq_cis_table_all_analysis_all_cancer_projects_cat.tab"
# results.dir <- "/home/jamondra/Downloads/Temp/xseq_tests/cis_prot_cod_genes"
# xseq.supp.mat.file <- "/home/jamondra/Downloads/Temp/xseq_tests/cis_prot_cod_genes/Xseq_supp_material_tables.Rdata"


###########################
## Create results folder ##
###########################
message("; Creating result folders")
folders.to.create <- list()
folders.to.create[["plots"]] <- file.path(results.dir, "plots")
folders.to.create[["tables"]] <- file.path(results.dir, "tables")
thrash <- sapply(folders.to.create, dir.create, recursive = TRUE, showWarnings = FALSE)


#############################
## Load xseq supp material ##
#############################
message("; Loading xseq supplementary material tables")
## Note: the object is called: xseq.sup.mat.tab
load(xseq.supp.mat.file)


#####################
## Load xseq table ##
#####################
message("; Loading xseq table: ", xseq.cis.merged.file)
xseq.cis.merged <- fread(xseq.cis.merged.file, header = TRUE, sep = "\t")

## Cast this columns to numeric values
xseq.cis.merged$prob_gene <- as.numeric(xseq.cis.merged$prob_gene)
xseq.cis.merged$prob_mut <- as.numeric(xseq.cis.merged$prob_mut)

## Count samples per project
nb.samples.project <- xseq.cis.merged %>%
                        subset(mut_class == "all") %>%
                        group_by(project_name) %>%
                        summarise(Nb_samples = n_distinct(sample))


## Select the significant genes
xseq.cis.merged <- as.data.table(subset(xseq.cis.merged, prob_gene >= 0.5))
xseq.cis.merged$shape_symb <- 19

xseq.cis.merged$gene <- as.factor(xseq.cis.merged$gene)
xseq.cis.merged$project_name <- as.factor(xseq.cis.merged$project_name)
xseq.cis.merged$mut_class <- as.factor(xseq.cis.merged$mut_class)


#############################
## Count the mutated genes ##
#############################

## For each gene on each project:
## 1) Get the line with the highest mut_prob and the max gene_prob
xseq.cis.merged.filt <- xseq.cis.merged %>% 
                          group_by(gene, project_name) %>%
                          filter(prob_gene == max(prob_gene)) %>%
                          filter(prob_mut == max(prob_mut))
# View(xseq.cis.merged.filt)


## Count in how many projects each gene was significant
freq.genes.diff.projects <- xseq.cis.merged.filt %>% 
                              group_by(gene) %>%
                              tally()
# View(freq.genes.diff.projects)


######################
## Draw the heatmap ##
######################
# heatmap.summary <- ggplot(xseq.cis.merged.filt, aes(x = gene, y = project_name, fill = prob_gene, height = prob_mut, width = prob_mut)) +
heatmap.summary <- ggplot(xseq.cis.merged.filt, aes(x = gene, y = project_name, fill = prob_gene)) +
                    geom_tile() +
                    coord_fixed() + 
                    scale_fill_continuous(low="#ffeda0", high="#d73027", guide="colorbar", na.value="white") +
                    theme_classic() +
                    labs(title = "", x = "Gene Symbol", y = "Project") +
                    theme(axis.text.x = element_text(angle = 45, hjust = 1.1,size=5,colour="black",face="bold"), axis.text.y = element_text(size=5,colour="black"), axis.title.y = element_text(size=5,colour = "black",face="bold",vjust=0.12)) +
                    theme(legend.text = element_text(size = 5), legend.title=element_text(size=5)) +
                    theme(legend.direction = "vertical", legend.position = "left") +
                    scale_x_discrete(limits = unlist(unique(xseq.cis.merged$gene))) +
                    scale_y_discrete(limits = unlist(unique(xseq.cis.merged$project))) +
                    coord_flip()

# ## Draw bar plot with number of samples per project
# nb.samples.project <- nb.samples.project[nb.samples.project$project_name %in% xseq.cis.merged.filt$project_name, ]
# nb.samples.plot <- ggplot(nb.samples.project, aes(y = Nb_samples, x = project_name)) +
#                     geom_bar(stat="identity", width=0.5) +
#                     theme_classic() +
#                     coord_flip() +
#                     theme(axis.text.x = element_text(angle = 45, hjust = 1,size=8,colour="black",face="bold"),
#                           axis.text.y = element_blank(),
#                           axis.title.y = element_blank(),
#                           axis.ticks.y = element_blank(),
#                           legend.text = element_text(size = 8))
# 
# 
# ggarrange(heatmap.summary, nb.samples.plot,
#           heights = c(4,4),
#           widths = c(15, 5),
#           ncol = 2,
#           nrow = 1, 
#           align = "h",
#           common.legend = TRUE,
#           legend = "left" )



## Export the plot
xseq.results.summary.plot <- file.path(folders.to.create[["plots"]], "xseq_cis_protein_coding_results_summary_plot.pdf")
message("; Exporting xseq summary plot: ", xseq.results.summary.plot)
ggsave(xseq.results.summary.plot, device = "pdf", width = 5, height = 10)
# ggsave(xseq.results.summary.plot, device = "pdf", width = 20, height = 5)
# ggsave(xseq.results.summary.plot, device = "pdf", limitsize = FALSE, width = 35, height = 10)

##############################################
## Export table with results and            ##
## comparison versus supplementary material ##
##############################################
projects <- unique(as.vector(xseq.cis.merged.filt$project_name))
xseq.cis.merged.filt.annotated <- data.frame()
for(p in projects){
  
  ## Select the lines corresponding to a given project
  xseq.selection <- subset(xseq.cis.merged.filt, project_name == p)
  
  ## Selected genes of a given project
  selected.genes <- as.vector(xseq.selection$gene)
  
  ## Remove the suffix '-US' from the project name
  ## This is required since project names in xseq results do not contain the suffix.
  p.parsed <- gsub(pattern = "-US", replacement = "", x = p)
  
  ## Check if the predicted genes (in this analysis) were reported in the original xseq publication
  ## 1) In cis analysis
  ## 2) As Bona fide cancer genes
  ## 3) As unknown cancer status genes
  ## 4) As false positive cancer genes
  selection.xseq.supp.mat.tumour.type <- subset(xseq.sup.mat.tab[["pred_genes_cis"]], Tumour_type == p.parsed)
  predictions.reported.cis <- selected.genes %in% selection.xseq.supp.mat.tumour.type$HGNC_symbol
  # predictions.reported.bonafide <- selected.genes %in% xseq.sup.mat.tab[["bona_fide_cancer_genes"]]$HGNC_symbol
  predictions.reported.bonafide <- selected.genes %in% xseq.sup.mat.tab[["cancer_gene_census"]]
  predictions.reported.unknown.cancer.status <- selected.genes %in% xseq.sup.mat.tab[["unknown_cancer_status_genes"]]
  predictions.reported.false.positive.cancer <- selected.genes %in% xseq.sup.mat.tab[["cancer_genes_false_positive"]]
  
  ## Calculate the percentage of genes reported as significant in this analysis
  ## relative to the number of reported genes in the xseq publication
  prediction.percentage <- sum(selection.xseq.supp.mat.tumour.type$HGNC_symbol %in% selected.genes) / length(selected.genes)
  prediction.percentage <- round(prediction.percentage, digits = 2)
  
  ## Check if the predicted genes are reported in the Cancer Gene Census
  selected.genes.cgc <- selected.genes %in% xseq.sup.mat.tab[["cancer_gene_census"]]
  selected.genes.cgc.perc <- round(sum(selected.genes.cgc)/length(selected.genes), digits = 2)
  
  ## Check if the predicted genes are reported in the Network Cancer Gene
  selected.genes.ncg <- selected.genes %in% xseq.sup.mat.tab[["network_cancer_gene"]]
  selected.genes.ncg.perc <- round(sum(selected.genes.ncg)/length(selected.genes), digits = 2)
  
    
  ## Add the columns
  xseq.selection$Supp_mat_cis <- predictions.reported.cis
  xseq.selection$Supp_mat_cis_overlap <- rep(prediction.percentage, times = nrow(xseq.selection))
  xseq.selection$Bona_fide <- predictions.reported.bonafide
  xseq.selection$Supp_mat_unknown_cancer_status <- predictions.reported.unknown.cancer.status
  xseq.selection$Supp_mat_false_positive_cancer_gene <- predictions.reported.false.positive.cancer
  xseq.selection$Cancer_Gene_Census <- selected.genes.cgc
  xseq.selection$Cancer_Gene_Census_ovrlp <- selected.genes.cgc.perc
  xseq.selection$Network_Cancer_Gene <- selected.genes.ncg
  xseq.selection$Network_Cancer_Gene_ovrlp <- selected.genes.ncg.perc
  xseq.selection <- as.data.frame(xseq.selection)
  
  ## Concatenate the results
  xseq.cis.merged.filt.annotated <- rbind(xseq.cis.merged.filt.annotated, xseq.selection)
}

## Export the table
xseq.results.annotated.file <- file.path(folders.to.create[["tables"]], "xseq_cis_protein_coding_results_with_annotations.tab")
message("; Exporting xseq prediction with annotations: ", xseq.results.annotated.file)
write.table(xseq.cis.merged.filt.annotated, file = xseq.results.annotated.file, sep = "\t", quote = FALSE, col.names = TRUE, row.names = FALSE)