#################################
## Load R packages + utilities ##
#################################
required.libraries <- c("amap",
                        "data.table",
                        "dplyr",
                        "future.apply",
                        "ggplot2",
                        "ggridges",
                        "ggthemes",
                        "gridExtra",
                        "ggpubr",
                        "reshape2",
                        "viridis")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    suppressPackageStartupMessages(library(lib, character.only=TRUE))
  }
}

source("/storage/mathelierarea/processed/jamondra/Projects/dysmir/R_scripts/Utilities/dysmiR_utilities.R")
# source("/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/R_scripts/Utilities/dysmiR_utilities.R")


###################################################################################################
## Read arguments from command line
##
## Some variables are mandatory: If they are not declared from the command line the program ends
###################################################################################################
message(";Reading arguments from command line")
args <- commandArgs(trailingOnly=TRUE)
if (length(args >= 1)) {
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
  }
}


if (!exists("miR.mRNA.network.file")) {
  stop("Missing mandatory argument (miRNA-mRNA regulatory network): miR.mRNA.network.file ")
} else if (!exists("mRNA.exp.tab.file")) {
  stop("Missing mandatory argument (mRNA expression table): mRNA.exp.tab.file ")
} else if (!exists("miRNA.exp.tab.file")) {
  stop("Missing mandatory argument (mRNA expression table): miRNA.exp.tab.file ")
} else if (!exists("mir.to.premir.tab.file")) {
  stop("Missing mandatory argument (pre-miRNA-miRNa association table file path): mir.to.premir.tab.file ")
} else if (!exists("results.dir")) {
  stop("Missing mandatory argument (Results folder): results.dir ")
} else if (!exists("n")) {
  stop("Missing mandatory argument (Number of negative controls): n ")
} else if (!exists("project.name")) {
  stop("Missing mandatory argument (Cancer Project name): project.name ")
}


## Debug
# miR.mRNA.network.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/Files_dor_debug/mir_correlation/targetScan_miRNA-mRNA_network.Rdata"
# miRNA.exp.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/Files_dor_debug/mir_correlation/ICGC_miRNA_expression_table_normalized_BRCA-US.tab"
# mRNA.exp.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/Files_dor_debug/mir_correlation/RNAseq_cpm_log2_expression_table_BRCA-US.tab"
# mir.to.premir.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/Files_dor_debug/mir_correlation/hsa_mir_to_premir.tsv"
# specimen.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/Files_dor_debug/mir_correlation/Specimen_Donor_information_Tumour_BRCA-US_WGS.tab"
# n <- 2
# project.name <- "BRCA-US"
# results.dir <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/Files_dor_debug/mir_correlation"
# 
# 
# ## Casting: from character (argument) to numeric
# n <- as.numeric(n)



#######################
## Defined functions ##
#######################

## Given a miR name, get its targets (from the miRNA <-> mRNA network)
## and compute two correlation coeficients: Pearson and Spearman.
## Export a table with the results.
correlation.miR.mRNAs <- function(miRNA, network){
  
  ## Rename columns
  colnames(network)[1:2] <- c("miR", "Target_gene")
  
  ## Get the query miR's target genes 
  mir.targets <- unique(network$Target_gene)
  
  ## Iterate over the target genes
  corr.df <- sapply(mir.targets, function(target){
    
    ## Get the expression values: miR + target gene
    mir.expr.values <- miRNA.exp.tab[,miRNA]
    target.expr.values <- mRNA.exp.tab[,target]
    
    ## Calculate Pearson correlation: miR vs target gene
    pearson.cor <- cor(x = mir.expr.values, 
                       y = target.expr.values,
                       method = "pearson")
    
    ## Calculate Spearman correlation: miR vs target gene
    spearman.cor <- cor(x = mir.expr.values, 
                        y = target.expr.values,
                        method = "spearman")
    
    ## Save this values in a data.frame
    return(c("Pearson" = pearson.cor,
             "Spearman" = spearman.cor))
    
  })
  corr.df <- t(corr.df)
  
  ## Melt the data.frame: convert it in a molten DF
  corr.df <- melt(corr.df)
  
  ## Add the column with the miR name
  corr.df$miR <- miRNA
  return(corr.df)
}


## Given a miR name, randomly choose the same number of targets (from the miRNA <-> mRNA network)
## and compute two correlation coeficients: Pearson and Spearman.
## The number of samples can be set by the user.
## Export a table with the results.
correlation.miR.mRNAs.neg.ctrl <- function(miRNA, network, n){
  
  ## Get the query miR's target genes 
  mir.targets <- unique(subset(network, miR == miRNA)$Target_gene)
  nb.mir.targets <- length(unique(mir.targets))
  
  ## Select a subsample of genes: 
  ## 1) same size thant the number of targets
  ## 2) do not consider the real targets
  ## 3) Repeat N times
  random.selection <- data.frame()
  for(i in 1:n){
    
    ## Randomly selected genes
    random.genes <- sample(selected.genes[!selected.genes %in% mir.targets], size = nb.mir.targets, replace = FALSE)
    
    ## 
    corr.df <- sapply(random.genes, function(target){
      
      ## Get the expression values: miR + target gene
      mir.expr.values <- miRNA.exp.tab[,miRNA]
      target.expr.values <- mRNA.exp.tab[,target]
      
      ## Calculate Pearson correlation: miR vs target gene
      pearson.cor <- cor(x = mir.expr.values, 
                         y = target.expr.values,
                         method = "pearson")
      
      ## Calculate Spearman correlation: miR vs target gene
      spearman.cor <- cor(x = mir.expr.values, 
                          y = target.expr.values,
                          method = "spearman")
      
      ## Save this values in a data.frame
      return(c("Pearson" = pearson.cor,
               "Spearman" = spearman.cor))
      
    })
    corr.df <- t(corr.df)
    
    ## Melt the data.frame: convert it in a molten DF
    corr.df <- melt(corr.df)
    
    corr.df$miR <- miRNA
    corr.df$type <- "Random"
    corr.df$neg_ctrl <- rep(i, each = nb.mir.targets)
    
    ## Concatenate the new DF after each iteration
    random.selection <- rbind(random.selection, corr.df)
  }
  return(random.selection)
}



###########################
## Create results folder ##
###########################
message("; Creating result folders")
out.folders <- list()
out.folders[["abs_cor"]] <- file.path(results.dir, "plots", "abs_cor")
out.folders[["miRNA_gene_cor"]] <- file.path(results.dir, "plots", "miRNA_gene_cor")
out.folders[["tables"]] <- file.path(results.dir, "tables")
thrash <- sapply(out.folders, dir.create, recursive = TRUE, showWarnings = FALSE)


####################################
## Load miRNA - mRNA network file ##
####################################
message("; Loading miRNA-mRNa network")
load(miR.mRNA.network.file) ## network.to.export
miR.mRNA.network <- network.to.export
rm(network.to.export)

## Convert the network: from xseq format to dataframe
miR.mRNA.tab <- lapply(names(miR.mRNA.network), function(l){
  
  ## Convert the list in a df
  net.df <- melt(miR.mRNA.network[l])
  net.df$Target <- rownames(net.df)
  
  ## Parse column names
  rownames(net.df) <- NULL
  colnames(net.df) <- c("Weight", "miRNA", "Target")
  net.df <- as.data.table(net.df)
  
})
miR.mRNA.tab <- rbindlist(miR.mRNA.tab)


#################################
## Load miRNA expression table ##
#################################
message("; Loading miRNA expression table")
miRNA.exp.tab <- read.csv(miRNA.exp.tab.file, header = TRUE, sep = "\t")
colnames(miRNA.exp.tab) <- gsub(x = colnames(miRNA.exp.tab), pattern = "\\.", replacement = "-")
# miRNA.exp.tab[1:5, 1:5]


################################
## Load mRNA expression table ##
################################
message("; Loading mRNA expression table")
mRNA.exp.tab <- read.csv(mRNA.exp.tab.file, header = TRUE, sep = "\t")
# colnames(miRNA.exp.tab) <- gsub(x = colnames(miRNA.exp.tab), pattern = "\\.", replacement = "-")
# mRNA.exp.tab[1:5, 1:5]


################################
## Load tumor speciment table ##
################################
message("; Loading tumor specimen table")
specimen.tab <- fread(specimen.tab.file)
specimen.tab <- as.vector(specimen.tab$icgc_specimen_id)


#################################
## Select samples intersection ##
#################################
samples.inters <- base::intersect(rownames(miRNA.exp.tab), rownames(mRNA.exp.tab))
samples.inters <- base::intersect(samples.inters, specimen.tab)
miRNA.exp.tab <- miRNA.exp.tab[samples.inters,]
mRNA.exp.tab <- mRNA.exp.tab[samples.inters,]


################################################
## Load miRNA <-> pre-miRNA association table ##
################################################
message("; Loading miR <-> premiR table")
mir.to.premir.tab <- fread(mir.to.premir.tab.file, header = FALSE, sep = "\t")
colnames(mir.to.premir.tab) <- c("miR", "premiR")

mir.to.premir.tab$miR <- tolower(mir.to.premir.tab$miR)
mir.to.premir.tab$premiR <- tolower(mir.to.premir.tab$premiR)
# mir.to.premir.tab$ID <- paste(mir.to.premir.tab$miR, mir.to.premir.tab$premiR, sep = "::")


###########################################
## Add miRNA columns in expression table ##
###########################################
## The problem is that ICGC have measured pre-miRNAs. Not the miRNAs individually.
## Those columns for pre-miRNAs producing two mature miRNAs will be duplicated.

# miRNA.exp.tab2 <- miRNA.exp.tab
# miRNA.exp.tab <- miRNA.exp.tab2
# miRNA.exp.tab[1:5, 1:5]

miRNA.exp.tab <- t(miRNA.exp.tab)
miRNA.exp.tab <- as.data.frame(miRNA.exp.tab)
miRNA.exp.tab$premiR <- rownames(miRNA.exp.tab)

miRNA.exp.tab <- merge(miRNA.exp.tab, mir.to.premir.tab, by = "premiR")
miRs.in.tab <- miRNA.exp.tab$miR

drops <- c("premiR", "miR")
miRNA.exp.tab <- miRNA.exp.tab[ , !(names(miRNA.exp.tab) %in% drops)]

miRNA.exp.tab <- t(miRNA.exp.tab)
colnames(miRNA.exp.tab) <- miRs.in.tab
colnames(miRNA.exp.tab) <- paste(colnames(miRNA.exp.tab), 1:ncol(miRNA.exp.tab), sep = "_")

# miRNA.exp.tab <- t(data.table(miRNA.exp.tab))
# rownames(miRNA.exp.tab) <- paste(rownames(miRNA.exp.tab), 1:length(rownames(miRNA.exp.tab)), sep = "_")

## Select the miRNAs in the expression data & in the network
mirnas.with.targets.info <- colnames(miRNA.exp.tab)[gsub(colnames(miRNA.exp.tab), pattern = "_\\d+$", replacement = "", perl = TRUE) %in% miR.mRNA.tab$miRNA]

# dim(miR.mRNA.tab)
# a <- miR.mRNA.tab[miR.mRNA.tab$Target %in% colnames(mRNA.exp.tab),]
# dim(a)
# b <- a[a$miRNA %in% gsub(colnames(miRNA.exp.tab), pattern = "_\\d+$", replacement = "", perl = TRUE),]
# dim(b)
# unique(a$miRNA)[unique(a$miRNA) %in% unique(gsub(colnames(miRNA.exp.tab), pattern = "_\\d+$", replacement = "", perl = TRUE))]


############################################################
## Correlation coefficient calculation by miRNA-gene pair ##
############################################################
message("; Calculating correlations")
plan(multiprocess, workers = 1)
mirna.gene.pair.cor.list <- NULL
# mirnas.with.targets.info2 <- mirnas.with.targets.info
mirna.gene.pair.cor.list <- future_sapply(mirnas.with.targets.info, function(miR.nb){
  
  message("; Calculating correlations for: ", miR.nb)
  # miR.nb <- "hsa-mir-155-5p_108"
  # miR <- "hsa-mir-155-5p"
  
  ## Clean miRNA name
  miR <- gsub(miR.nb, pattern = "_\\d+$", replacement = "", perl = TRUE)
  
  ## Get the miRNA targets (keep those within the RNA-seq expression matrix)
  miR.target.names <- as.vector(subset(miR.mRNA.tab, miRNA == miR)$Target)
  miR.target.names <- miR.target.names[miR.target.names %in% colnames(mRNA.exp.tab)]
  
  ## Get the expression values for the query miRNA
  mir.expr.vector <- as.vector(miRNA.exp.tab[,miR.nb])
  
  plan(multiprocess, workers = 1)
  # g <- "E2F2"
  mirna.gene.cor <- future_sapply(miR.target.names, function(g){
    
    
    target.expr.vector <- as.vector(mRNA.exp.tab[,g])
    
    ## Calculate spearman's rho and p-value
    mirna.gene.pair.cor.test <- cor.test(x = mir.expr.vector,
                                    y = target.expr.vector,
                                    method = "spearman",
                                    alternative = "two.sided")
    mirna.gene.pair.cor.rho <- as.vector(unlist(mirna.gene.pair.cor.test[4]))
    mirna.gene.pair.cor.pval <- as.vector(unlist(mirna.gene.pair.cor.test[3]))
    mirna.gene.pair.cor.eval <- mirna.gene.pair.cor.pval * nrow(miR.mRNA.tab)
    mirna.gene.pair.cor.sig <- round(-log10(mirna.gene.pair.cor.eval), digits = 2)
    
    ## Draw the correlation plots for significant miRNA-target paris
    if(mirna.gene.pair.cor.sig >= 3){
      
      expr.df <- data.frame(RNA_seq = target.expr.vector,
                            sRNA_seq = mir.expr.vector)
      
      cor.plot <- ggplot(data = expr.df, aes(x = sRNA_seq, y = RNA_seq)) +
                    geom_point(colour = "#377eb8", size = 2) +
                    xlim(c(0, max(expr.df))) +
                    ylim(c(0, max(expr.df))) +
                    theme_pubr() +
                    labs(title = paste0("Expression correlation - ", miR, " vs ", g), x = miR, y = g)
      cor.plot.file <- file.path(out.folders[["miRNA_gene_cor"]], paste("Correlation_plot_", miR, "_vs_", g , ".pdf"))
      ggsave(plot = cor.plot, filename = cor.plot.file, device = "pdf", width = 6, height = 6)
    }
    
    data.table(miRNA = miR,
               gene = g,
               Spearman = mirna.gene.pair.cor.rho,
               P_val = mirna.gene.pair.cor.pval,
               E_val = mirna.gene.pair.cor.eval,
               Significance = mirna.gene.pair.cor.sig,
               Project = project.name,
               miRNA_ID = miR.nb)
  })
  mirna.gene.cor <- as.data.table(t(data.frame(mirna.gene.cor)))
  mirna.gene.cor
})
mirna.gene.pair.cor.tab <- do.call(rbind, mirna.gene.pair.cor.list)
# mirna.gene.pair.cor.tab <- rbindlist(as.data.frame(mirna.gene.pair.cor.list))
mirna.gene.pair.cor.tab$miRNA <- unlist(mirna.gene.pair.cor.tab$miRNA)
mirna.gene.pair.cor.tab$gene <- unlist(mirna.gene.pair.cor.tab$gene)
mirna.gene.pair.cor.tab$Spearman <- unlist(mirna.gene.pair.cor.tab$Spearman)
mirna.gene.pair.cor.tab$Project <- unlist(mirna.gene.pair.cor.tab$Project)
mirna.gene.pair.cor.tab$miRNA_ID <- unlist(mirna.gene.pair.cor.tab$miRNA_ID)
mirna.gene.pair.cor.tab$P_val <- unlist(mirna.gene.pair.cor.tab$P_val)
mirna.gene.pair.cor.tab$E_val <- unlist(mirna.gene.pair.cor.tab$E_val)
mirna.gene.pair.cor.tab$Significance <- unlist(mirna.gene.pair.cor.tab$Significance)


## Absolute value of Spearman cor
message("; Calculating correlation stats")
# mirna.gene.pair.cor.tab <- mirna.gene.pair.cor.tab %>%
#                             mutate(Spearman_abs = round(abs(Spearman), digits = 2)) %>%
#                             group_by(miRNA_ID) %>%
#                             mutate(Median_abs_spearman = median(Spearman_abs, na.rm = TRUE),
#                                    Mean_abs_spearman = mean(Spearman_abs, na.rm = TRUE),
#                                    Max_cor = max(Spearman),
#                                    Min_cor = min(Spearman))

mirna.gene.pair.cor.tab <- mirna.gene.pair.cor.tab %>%
                            group_by(miRNA_ID) %>%
                            mutate(Max_cor = max(Spearman),
                                   Min_cor = min(Spearman),
                                   Nb_targets = n())

# mirna.gene.pair.cor.tab <- mirna.gene.pair.cor.tab[1:20000,]
mirna.gene.pair.cor.tab <- mirna.gene.pair.cor.tab[order(mirna.gene.pair.cor.tab$Significance, mirna.gene.pair.cor.tab$Spearman, decreasing = TRUE),]
ordered.labels.by.expr <- unique(mirna.gene.pair.cor.tab$miRNA)

# 
# ####################################################################################
# ## Plot correlation values distribution for every miRNA-gene network: real values ##
# ####################################################################################
# message("; Plotting correlation real values")
# mirna.cor.plot <- ggplot(data = mirna.gene.pair.cor.tab, aes(x = Spearman, y = miRNA, fill = Mean_abs_spearman)) +
#   # geom_density_ridges2(jittered_points = TRUE, alpha = 0.5, scale = 0.95, point_shape = "|", point_size = 3, size = 0.25, position = position_points_jitter(height = 0)) +
#   geom_density_ridges2(jittered_points = FALSE, alpha = 0.5, scale = 0.85) +
#   scale_fill_viridis(option = "magma", direction = -1) +
#   # ggtitle(paste0("sRNA-seq expression distribution:\n ", g, " in ", project.name, " - ", ms, " mutations")) +
#   xlim(c(-1,1)) +
#   theme_classic() +
#   scale_y_discrete(limits = rev(ordered.labels.by.expr))
# mirna.cor.plot.pdf <- file.path(out.folders[["abs_cor"]], paste0("miRNA_target_gene_expression_real_correlations_", project.name, ".pdf"))
# ggsave(plot = mirna.cor.plot, filename = mirna.cor.plot.pdf, device = "pdf", width = 7, height = 50, limitsize = FALSE)
# 
# 
# ########################################################################################
# ## Plot correlation values distribution for every miRNA-gene network: absolute values ##
# ########################################################################################
# message("; Plotting correlation absolute values")
# mirna.abs.cor.plot <- ggplot(data = mirna.gene.pair.cor.tab, aes(x = Spearman_abs, y = miRNA, fill = Mean_abs_spearman)) +
#   # geom_density_ridges2(jittered_points = TRUE, alpha = 0.5, scale = 0.95, point_shape = "|", point_size = 3, size = 0.25, position = position_points_jitter(height = 0)) +
#   geom_density_ridges2(jittered_points = FALSE, alpha = 0.75, scale = 0.95) +
#   scale_fill_viridis(option = "magma", direction = -1) +
#   # ggtitle(paste0("sRNA-seq expression distribution:\n ", g, " in ", project.name, " - ", ms, " mutations")) +
#   xlim(c(0,1)) +
#   theme_classic() +
#   scale_y_discrete(limits = rev(ordered.labels.by.expr))
# mirna.abs.cor.pdf <- file.path(out.folders[["abs_cor"]], paste0("miRNA_target_gene_expression_absolute_correlations_", project.name, ".pdf"))
# ggsave(plot = mirna.abs.cor.plot, filename = mirna.abs.cor.pdf, device = "pdf", width = 7, height = 50, limitsize = FALSE)


######################
## Export cor table ##
######################
message("; Exporting correlation tables")
mirna.gene.pair.cor.tab.file <- file.path(out.folders[["tables"]], paste0("miRNA_target_correlations_", project.name, ".tab"))
fwrite(mirna.gene.pair.cor.tab, file = mirna.gene.pair.cor.tab.file, quote = FALSE, col.names = TRUE, row.names = FALSE)