#####################
## Load R packages ##
#####################
required.libraries <- c("dplyr", "ggplot2", "xseq")
for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    library(lib, character.only=TRUE)
  }
}

## miR expression table 399
expr.miR.399.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/dysmiR/Parse_tables/miRNA/miRNA_399_samples_normalized_BASIS_sample_ids_removed_controlspots.txt"
expr.miR.399.tab <- read.csv(expr.miR.399.tab.file, sep = "\t", header = TRUE)
colnames(expr.miR.399.tab) <- as.vector(sapply(names(expr.miR.399.tab), function(x){ gsub("X", "", x)}))
rownames(expr.miR.399.tab) <- expr.miR.399.tab[,1]
expr.miR.399.tab <- expr.miR.399.tab[,-1]
expr.miR.399.tab <- t(expr.miR.399.tab)
expr.miR.399.tab <- as.matrix(expr.miR.399.tab)
colnames(expr.miR.399.tab) <- tolower(colnames(expr.miR.399.tab))

## miR expression table 295
expr.miR.295.tab.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/miR_expression_median_centered_using_detected_commonMinimum_295.txt"
expr.miR.295.tab <- read.csv(expr.miR.295.tab.file, sep = "\t", header = TRUE)
colnames(expr.miR.295.tab) <- as.vector(sapply(names(expr.miR.295.tab), function(x){ gsub("X", "", x)}))
rownames(expr.miR.295.tab) <- expr.miR.295.tab[,1]
expr.miR.295.tab <- expr.miR.295.tab[,-1]
expr.miR.295.tab <- t(expr.miR.295.tab)
expr.miR.295.tab <- as.matrix(expr.miR.295.tab)
colnames(expr.miR.295.tab) <- tolower(colnames(expr.miR.295.tab))

##########################################
## Load MIMAT <-> miR association table ##
##########################################
mimat.mir.key.file <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/key_MIMAT_miRNA.txt"
mimat.mir.key <- read.csv(mimat.mir.key.file, sep = "\t", header = TRUE)
colnames(mimat.mir.key) <- c("MIMAT", "miR")
mimat.mir.key$MIMAT <- tolower(mimat.mir.key$MIMAT)
mimat.mir.key$miR <- tolower(mimat.mir.key$miR)

## Selected miRs
miRs <- c("hsa-mir-409-3p", "hsa-mir-218-5p", "hsa-mir-654-3p", "hsa-mir-154-5p", "hsa-mir-381-3p", "hsa-mir-4419a")

## Plot the miR expressio distribution in the two expression tables

pdf("/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/results/Exp_values_selected_miRs_399_vs_295.pdf")
sapply(miRs, function(m){
  
  print(m)
  
  mimat <- subset(mimat.mir.key, miR == m)$MIMAT
  
  ## 
  exp.295 <- expr.miR.295.tab[,mimat]
  exp.399 <- expr.miR.399.tab[,m]
  u.test <- wilcox.test(x = exp.295,
              y = exp.399)
  u.pval <- u.test[[3]] * length(miRs)
  u.pval <- prettyNum(u.pval, digits = 3)
  
  par(mfrow=c(2,1))
  hist(x = expr.miR.399.tab[,m],
       breaks = 25,
       freq = FALSE,
       xlab = "Expression values",
       main = paste(m, "- 399 samples - U-test:", u.pval))
  
  hist(x = expr.miR.295.tab[,mimat],
       breaks = 25,
       freq = FALSE,
       xlab = "Expression values",
       main = paste(m, "- 295 samples - U-test:", u.pval))
  


})
dev.off()
