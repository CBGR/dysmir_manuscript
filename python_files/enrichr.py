#!/usr/bin/python
#*-* coding: utf-8 *-*

""" Compute functional enrichment from genes list using Enrichr. """

import sys
import os
import getopt
import cookielib
import poster
import urllib2


URL = "http://amp.pharm.mssm.edu/Enrichr/"
ENRICH_SETS = ["KEGG_2016", "WikiPathways_2016", "Panther_2016",
               "GO_Biological_Process_2018", "GO_Molecular_Function_2018",
               "OMIM_Disease", "OMIM_Expanded"]


def get_genes(input_file):
    with open(input_file) as stream:
        return '\t'.join(stream.readlines()[1:])


def get_func_significant(results):
    significant = []
    for line in results.split('\n'):
        spl = line.split('\t')
        if spl[0] == "Term":
            significant.append(line)
        elif line:
            if eval(spl[3]) < 0.05:
                significant.append(line)
    return '\n'.join(significant)


def enrichr(genes, output_dir, basename, description):
    opener = poster.streaminghttp.register_openers()
    opener.add_handler(urllib2.HTTPCookieProcessor(cookielib.CookieJar()))
    parpost = {"list": genes, "description": description}
    datagen, headers = poster.encode.multipart_encode(parpost)
    request = urllib2.Request("{0}/enrich".format(URL), datagen, headers)
    urllib2.urlopen(request)
    for set_to_analyze in ENRICH_SETS:
        output_file = "{0}/{1}_{2}.tsv".format(output_dir, basename,
                                               set_to_analyze)
        res_url = "{0}/export?backgroundType={1}&filename={2}".format(
            URL, set_to_analyze, output_file)
        xres = urllib2.urlopen(res_url)
        results = xres.read()
        with open(output_file, 'w') as stream:
            stream.write(results)
        significant = get_func_significant(results)
        output_file = "{0}/{1}_{2}_significant.tsv".format(output_dir,
                                                           basename,
                                                           set_to_analyze)
        with open(output_file, 'w') as stream:
            stream.write(significant)


def main():
    usage = '''
    %s -i <input file with genes> -o <output dir> [-d <description]
    ''' % (sys.argv[0])

    try:
        opts, _ = getopt.getopt(sys.argv[1:], "i:o:d:h")
    except getopt.GetoptError:
        sys.exit(str(getopt.GetoptError) + usage)

    input_file = None
    output_dir = None
    description = "description"
    for o, a in opts:
        if o == "-i":
            input_file = a
        elif o == "-o":
            output_dir = a
        elif o == "-d":
            description = a
        else:
            sys.exit(usage)

    if not(input_file and output_dir):
        sys.exit(usage)

    genes = get_genes(input_file)
    base = os.path.splitext(os.path.basename(input_file))[0]
    enrichr(genes, output_dir, base, description)


###############################################################################
#                               MAIN
###############################################################################
if __name__ == "__main__":
    main()
