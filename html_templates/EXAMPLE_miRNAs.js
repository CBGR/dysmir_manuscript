// import our plot-library
var mutneedles = require("muts-needle-plot");

var colorMap = {
  
  // mutation categories
  --mut_class--
  
};

var legends = {
  x: "Genomic coordinates",
  y: "Number of mutations"
};

//Crate config Object
var plotConfig = {
  maxCoord :      --end--,
  minCoord :      --start--,
  targetElement : yourDiv,
  mutationData:   "--mutations_json--",
  regionData:     "--regions_json--",
  colorMap:       colorMap,
  legends:        legends
};

// Instantiate a plot
var instance = new mutneedles(plotConfig);

//@biojs-instance=instance (provides the instance to the BioJS event system)
